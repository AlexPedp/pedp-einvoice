package com.progettoedp.properties;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class AppProperties {

	public String partitaIva;
	public String applicationType;
	public String databaseApplicationType;
	public String databaseApplicationName;
	public String hostApplicationName;
	public String instanceApplicationName;
	public String portApplicationName;
	public String userApplicationName;
	public String passwordApplicationName;
	public String hostName;
	public String portNumber;
	public String databaseName;
	public String user;
	public String password;
	public String xmlPath;
	public String pdfPath;
	public String tableName;
	public String tableNameEq;
	public String attachPath;
	public String backupPath;
	public String printerName;
	public String secondPrinterName;
	public String host_as400;
	public String user_as400;
	public String password_as400;
	public String library_as400;
	public String databaseNameEq;
	public Integer subbedDays;
	public boolean activeInvoice;
	public String ixUserName;
	public String ixPassword;
	public String ixClientId;
	public String ixClientSecret;
	public String library_as400_cont;
	public String tipo_protocollo;
	public String xml_as400_path;
	public String filteredDocuments;
	public boolean ftp_as400;
	
	private Map<String, String> map = new HashMap<String, String>();
	private static AppProperties instance = null;

	public AppProperties() {
		
	}
	
	public void setDefault(){
		partitaIva = "00178340261";
		applicationType =  "FATEL";
		databaseApplicationType =   "SQLITE";
		databaseApplicationName =  "192.168.50.1";
		hostApplicationName =  "Pedp_eInvoice";
		instanceApplicationName =  "SQLEXPRESS08";
		portApplicationName =  "1433";
		userApplicationName =  "sa";
		passwordApplicationName =   "pw1pedp";
		hostName =  "192.168.50.6";
		portNumber =  "1433";
		databaseName = "FATEL";
		user =  "sa";
		password =  "milkuht";
		xmlPath =  "\\\\192.168.50.6\\fatel\\documenti\\xml\\sol1g\\";
		pdfPath =  "\\\\192.168.50.6\\pdf\\";
		tableName =  "SOL1GFAT_MAST";
		tableNameEq =  "STARMOVFATT4_m";
		attachPath =  "\\\\192.168.50.6\\attach\\";
		backupPath =  "\\\\192.168.50.6\\backup\\";
		printerName =  "FS3750";
		secondPrinterName =  "FS3750";
		host_as400 =  "192.168.50.1";
		user_as400 =  "ITACA";
		password_as400 = "ACATI";
		library_as400 =  "IT_SOL_F";
		databaseNameEq =  "STAR_SOLIGO";
		subbedDays = 0;
		activeInvoice = false;
		ixUserName =  "fabio.stocco@progettoedp.it";
		ixPassword =  "!GWf!K2Y";
		ixClientId =  "d0e6e5e03fc24e4c9316e9c1b669c490";
		ixClientSecret = "BAwyfrX7zHAA4hAstMw4eZRkchGuwi4w";
		library_as400_cont = "BMS60L_DAT";
		tipo_protocollo = "ITACA";
		xml_as400_path = "\\\\192.168.50.1\\coge\\SOL\\FattElettronica\\Passive\\";
		ftp_as400 = true;
		filteredDocuments =  "TD16|TD17|TD18|TD19";
		
	}

	public void create(File file, boolean isDefault){

		Properties prop = new Properties();
		OutputStream output = null;

		try {

			output = new FileOutputStream(file);
			if (isDefault)
				setDefault();
			
			prop.setProperty("partita_iva", partitaIva);
			prop.setProperty("application_type", applicationType);
			prop.setProperty("host_name", hostName);
			prop.setProperty("host_as400", host_as400);
			prop.setProperty("port_number", portNumber);
			prop.setProperty("database_name", databaseName);
			prop.setProperty("user_as400", user_as400);
			prop.setProperty("password_as400", password_as400);
			prop.setProperty("library_as400", library_as400);
			prop.setProperty("database_eq", databaseNameEq);
			prop.setProperty("database_application_type", databaseApplicationType);
			prop.setProperty("database_application_name", databaseApplicationName);
			prop.setProperty("host_application_name", hostApplicationName);
			prop.setProperty("instance_application_name", instanceApplicationName);
			prop.setProperty("port_application_name", portApplicationName);
			prop.setProperty("user_application_name", userApplicationName);
			prop.setProperty("password_application_name", passwordApplicationName);
			prop.setProperty("printer_name", printerName);
			prop.setProperty("second_printer_name", secondPrinterName);
			prop.setProperty("table_name", tableName);
			prop.setProperty("table_name_eq", tableNameEq);
			prop.setProperty("user_name", user);
			prop.setProperty("password", password);
			prop.setProperty("xml_path", xmlPath);
			prop.setProperty("pdf_path", pdfPath);
			prop.setProperty("attach_path", attachPath);
			prop.setProperty("backup_path", backupPath);
			prop.setProperty("sub_days", Integer.toString(subbedDays));
			prop.setProperty("active_invoice", Boolean.toString(activeInvoice));
			prop.setProperty("ix_user_name", ixUserName);
			prop.setProperty("ix_password", ixPassword);
			prop.setProperty("ix_client_id", ixClientId);
			prop.setProperty("ix_client_secret", ixClientSecret);
			prop.setProperty("library_as400_cont", library_as400_cont);
			prop.setProperty("tipo_protocollo", tipo_protocollo);
			prop.setProperty("xml_as400_path", xml_as400_path);
			prop.setProperty("ftp_as400", Boolean.toString(ftp_as400));
			prop.setProperty("filtro_tipo_documento", filteredDocuments);
		
			// save properties to project root folder
			prop.store(output, null);

		} catch (IOException io) {
			io.printStackTrace();
		} finally {
			if (output != null) {
				try {
					output.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		}
	}
	 

	public void load(File file){

		Properties prop = new Properties();
		InputStream input = null;

		try {

			input = new FileInputStream(file);

			// load a properties file
			prop.load(input);

			for (final String name: prop.stringPropertyNames()) {
				map.put(name, prop.getProperty(name));
			}


		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		initComponent();
 
	}

	public void initComponent(){
	 
		partitaIva = map.get("partita_iva");
		applicationType = map.get("application_type");
		databaseApplicationType =  map.get("database_application_type");
		databaseApplicationName = map.get("database_application_name");
		hostApplicationName = map.get("host_application_name");
		instanceApplicationName = map.get("instance_application_name");
		portApplicationName = map.get("port_application_name");
		userApplicationName =  map.get("user_application_name");
		passwordApplicationName =  map.get("password_application_name");
		hostName = map.get("host_name");
		portNumber = map.get("port_number");
		databaseName = map.get("database_name");
		user = map.get("user_name");
		password = map.get("password");
		xmlPath = map.get("xml_path");
		pdfPath = map.get("pdf_path");
		tableName = map.get("table_name");
		tableNameEq = map.get("table_name_eq");
		attachPath = map.get("attach_path");
		backupPath = map.get("backup_path");
		printerName = map.get("printer_name");
		secondPrinterName = map.get("second_printer_name");
		host_as400 = map.get("host_as400");
		user_as400 = map.get("user_as400");
		password_as400 = map.get("password_as400");
		library_as400 = map.get("library_as400");
		databaseNameEq = map.get("database_eq");
		subbedDays = Integer.parseInt(map.get("sub_days"));
		activeInvoice = Boolean.parseBoolean(map.get("active_invoice"));
		ixUserName = map.get("ix_user_name");
		ixPassword = map.get("ix_password");
		ixClientId = map.get("ix_client_id");
		ixClientSecret = map.get("ix_client_secret");
		library_as400_cont = map.get("library_as400_cont");
		tipo_protocollo = map.get("tipo_protocollo");
		xml_as400_path = map.get("xml_as400_path");
		ftp_as400 = Boolean.parseBoolean(map.get("ftp_as400"));
		filteredDocuments = map.get("filtro_tipo_documento");
	}
 
	
}

