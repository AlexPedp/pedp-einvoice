package com.progettoedp.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.io.FileUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.itextpdf.html2pdf.ConverterProperties;
import com.itextpdf.html2pdf.HtmlConverter;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.pdf.WriterProperties;
import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfCopy;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfSmartCopy;
import com.progettoedp.data.Fattura;
import com.progettoedp.data.FileType.TYPE;
import com.progettoedp.logic.Import;
import com.progettoedp.util.Logger;

import sun.misc.BASE64Decoder;


public class XMLModel {

	private static final Logger logger = Logger.getLogger(Import.class);
	private static final String TAG = "XMLModel";
	private File xlsFile = new File("FoglioStileAssoSoftware.xsl");
	private File xmlFile;
	//	private File htmFile = new File("output.html");
	//	private File pdfFile = new File("output.pdf");
	private File htmFile;
	private File pdfFile;
	private File attachmentFile;
	private File pdfFileAll;
	private File htmFileTemp;
	private Fattura fattura;
	private HashMap<TYPE, File> appFiles = new HashMap<TYPE, File>();

	public XMLModel(File xmlFile, Fattura fattura, HashMap<TYPE, File> appFiles){
		this.fattura = fattura;
		this.xmlFile = xmlFile;
		this.appFiles = appFiles;
		init();
	}

	public void init(){

		for (Map.Entry<TYPE, File> item : appFiles.entrySet()) {
			TYPE key = item.getKey();
			File value = item.getValue();
			switch (key) {
			case HTML:
				htmFile = value;
				break;
			case TEMP:
				htmFileTemp = value;
				break;
			case PDF:
				pdfFile = value;
				break;
			case ATTACHMENT:
				attachmentFile = value;
				break;
			case ALL:
				pdfFileAll = value;
				break;
			default:
				break;
			}
		}

	}

	public List<File> run(){
		
		FileInputStream fis = null;
		List<File> files = new ArrayList<File>();

		try {

			TransformerFactory factory = TransformerFactory.newInstance();
			Source xslt = new StreamSource(xlsFile);
			Transformer transformer = factory.newTransformer(xslt);

			Source text = new StreamSource(xmlFile);
			transformer.transform(text, new StreamResult(htmFile));

			String readFileToString = FileUtils.readFileToString(htmFile, "UTF-8"); 
			//			System.out.println(readFileToString);
			
			File newHtml = addTagData(readFileToString);

			FileOutputStream outputStream = new FileOutputStream(pdfFile);
			WriterProperties writerProperties = new WriterProperties();
			writerProperties.addXmpMetadata();
			PdfWriter pdfWriter = new PdfWriter(outputStream, writerProperties);
			PdfDocument pdfDoc = new PdfDocument(pdfWriter);
			ConverterProperties props = new ConverterProperties();
			
			fis = new FileInputStream(newHtml);
			
			HtmlConverter.convertToPdf(fis, pdfDoc, props);
			pdfDoc.close();
			files.add(pdfFile);


			//	        List<IElement> elements = HtmlConverter.convertToElements(new FileInputStream(htmFile), props);
			//	        pdfDoc.setTagged();
			//	        com.itextpdf.layout.Document document = new  com.itextpdf.layout.Document(pdfDoc);
			//	        document.setMargins(50, 50, 50, 50);
			//	        for (IElement element : elements) {
			//	            document.add((IBlockElement)element);
			//	        }
			//	        document.close();


			if (fattura.getAllegato() != null){
				BASE64Decoder decoder = new BASE64Decoder();
				byte[] decodedBytes = decoder.decodeBuffer(fattura.getAllegato());
				FileOutputStream fop = new FileOutputStream(attachmentFile);
				fop.write(decodedBytes);
				fop.flush();
				fop.close();
				files.add(attachmentFile);

				//		        List<File> listOfPdfFiles = new ArrayList<File>();
				//		        listOfPdfFiles.add(pdfFile);
				//		        listOfPdfFiles.add(attachmentFile);
				//		        concatenatePdfs(listOfPdfFiles, pdfFileAll);

			}



		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
		   try {
			fis.close();
		} catch (Exception e) {
			logger.error(TAG, e);
			}
		}
		


		return files;

	}

	public void concatenatePdfs(List<File> listOfPdfFiles, File outputFile)  {

		try {

			Document document = new Document();
			FileOutputStream outputStream = new FileOutputStream(outputFile);
			PdfCopy copy = new PdfSmartCopy(document, outputStream);
			document.open();
			for (File inFile : listOfPdfFiles) {
				PdfReader reader = new PdfReader(inFile.getAbsolutePath());
				copy.addDocument(reader);
				reader.close();
			}
			document.close();
		} catch (Exception e) {

		}

	}

	public File addTagData(String htmlString) {
		try {
			
			String dataAccettazione = null;
			if (fattura.getDataRitiro()!= null){
				dataAccettazione = fattura.getDataRitiro();
			} else {
				dataAccettazione = fattura.getDataSdi2();
			}

			String idSdi = fattura.getIdSdi();

			String tag = "<label class=\"headerLabel\">id SDI: " + idSdi.trim() +  " Data Ricezione: " + dataAccettazione.substring(0,10);

			if (fattura.getNomeXml() != null){
				tag = tag +  " Nome XML: " + fattura.getNomeXml();
			}

			if (fattura.getNumProEq() != null && fattura.getNumProEq().length()>0){
				tag = tag +   "</br>" + " Numero Protocollo: " + fattura.getNumProEq();
			}

			if (fattura.getDataRegistrazione() != null && fattura.getDataRegistrazione().length()>0){
				tag = tag  + " Data Registrazione: " + fattura.getDataRegistrazione();
			}

			if (fattura.getRegistroIva() != null && fattura.getRegistroIva().length()>0){
				tag = tag  + " Registro Iva: " + fattura.getRegistroIva();
			}

			if (fattura.getCodice() != null && fattura.getCodice().length()>0){
				tag = tag  + "</br>" + " Codice: " + fattura.getCodice();
				if (fattura.getRagioneSocialeGestionale() != null && fattura.getRagioneSocialeGestionale().length()>0){
					tag = tag  + "-" + fattura.getRagioneSocialeGestionale();
				}
			}

			tag = tag + "</label><div style=\"height:20px\"></div>";
			org.jsoup.nodes.Document doc = Jsoup.parse(htmlString);
			Elements elms = doc.select("body");
			for (Element e : elms) {
				e.before(tag);
			}
 			
 			String newHtmlString = doc.html();
 			FileUtils.writeStringToFile(htmFileTemp, newHtmlString, "UTF-8");
 			return htmFileTemp;
 			 
		} catch (Exception e){
			return null;
		} finally {
			   
		}
		
		
	}

}
