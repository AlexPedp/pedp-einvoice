package com.progettoedp.dao;

import java.sql.Connection;


import java.sql.DriverManager;
import java.sql.SQLException; 

public class TestMssql {

	public static void main(String[] argv)    {

		System.out.println("-------- MySQL JDBC Connection Testing ------------");

		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		} catch (ClassNotFoundException e) {

			System.out.println("Where is your MySQL JDBC Driver?");
			e.printStackTrace();
			return;
		}

		System.out.println("MySQL JDBC Driver Registered!");
		Connection connection = null;

		try	{

			String url = "jdbc:sqlserver://192.168.50.6;Database=FATEL";

			String username = "sa";
			String password = "milkuht";
			connection = DriverManager.getConnection(url, username, password);

		} 

		catch (SQLException e)	{
			System.out.println("Connection Failed!");
			e.printStackTrace();
			return;
		}

		if (connection != null) 	{
			System.out.println("Fully connected.");
		} 

		else 	{
			System.out.println("Failed to make connection!");
		}
	}

}
