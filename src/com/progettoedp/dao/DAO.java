package com.progettoedp.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import com.progettoedp.data.Database;
import com.progettoedp.data.Euroquote;
import com.progettoedp.data.Fattura;
import com.progettoedp.data.Fornitore;
import com.progettoedp.data.Registrazione;
import com.progettoedp.data.Sdi;
import com.progettoedp.main.Application;
import com.progettoedp.testo.Testo;
import com.progettoedp.util.Logger;


public class DAO {

	private static final Logger logger = Logger.getLogger(DAO.class);

	private static final String TAG = "DAO";


	public static String type;

	static final String TABLE_FATTURE = "FATTURE";

	private DatabaseManager databaseManager;
	private Database database;

	public DAO(){

	}


	public void setDatabaseManager(DatabaseManager dm){
		this.databaseManager = dm;
	}


	public void init(Database database) {

		Connection conn = databaseManager.getConnection(database);
		String tableName = null;
		String create = null;
		this.database = database;

		try {
		

			switch (database.getType()) {
			case Testo.MSSQL:
				String catalog = conn.getCatalog();
				tableName = catalog + ".dbo." + TABLE_FATTURE; 
				create = "CREATE TABLE " + tableName 
						+ "( ID              INT           NOT NULL    IDENTITY    PRIMARY KEY, "
						+ "DATA_CREAZIONE VARCHAR(20), "
						+ "DATA_VARIAZIONE VARCHAR(20), "
						+ "AZIENDA VARCHAR(50), "
						+ "ATT_PASS VARCHAR(10), "
						+ "TIPO VARCHAR(10), "
						+ "NUMERO VARCHAR(50), "
						+ "DATA VARCHAR(20), "
						+ "PARTITA_IVA VARCHAR(50), "
						+ "RAGIONE_SOCIALE VARCHAR(100), "
						+ "TOTALE DECIMAL(20, 6), "
						+ "ALLEGATO VARCHAR(MAX), "
						+ "DETTAGLI VARCHAR(MAX), "
						+ "RIEPILOGHI VARCHAR(MAX), "
						+ "PAGAMENTI VARCHAR(MAX), "
						+ "DATA_RITIRO VARCHAR(30), "
						+ "DATA_SDI_1 VARCHAR(30), "
						+ "DATA_SDI_2 VARCHAR(30), "
						+ "ID_SDI VARCHAR(50), "
						+ "NOME_XML VARCHAR(50), "
						+ "NUMERO_PRO_EQ VARCHAR(50), "
						+ "DATA_PRO_EQ VARCHAR(20), "
						+ "DATA_RICEZIONE VARCHAR(20), "
						+ "DATA_CONSEGNA VARCHAR(20), "
						+ "TOKEN_IX VARCHAR(1000), "
						+ "AOOS_IX VARCHAR(100), "
						+ "UOS_IX VARCHAR(100), "
						+ "SUBSCRIPTION_IX VARCHAR(100), "
						+ "INVOICE_IX VARCHAR(100), "
						+ "STATO_SDI VARCHAR(50), "
						+ "STATO VARCHAR(50)"
						+ ")";

				break;
			case Testo.SQLITE:


				create = "CREATE TABLE " + TABLE_FATTURE
				+ " (" + "ID INTEGER  PRIMARY KEY AUTOINCREMENT NOT NULL, "
				+ "DATA_CREAZIONE TEXT," + "DATA_VARIAZIONE TEXT," 
				+ "AZIENDA TEXT," + "ATT_PASS TEXT," + "TIPO TEXT, " + "NUMERO TEXT, " + "DATA TEXT, "
				+ "PARTITA_IVA TEXT," + "RAGIONE_SOCIALE TEXT, " + "TOTALE REAL, " + "ALLEGATO TEXT,"
				+ "DETTAGLI TEXT, " + "RIEPILOGHI TEXT, " + "PAGAMENTI TEXT,"   
				+ "DATA_RITIRO TEXT, " + "DATA_SDI_1 TEXT, " + "DATA_SDI_2 TEXT, " + "ID_SDI TEXT, "
				+ "NOME_XML TEXT," + "NUMERO_PRO_EQ TEXT," + "DATA_PRO_EQ TEXT," 
				+ "DATA_RICEZIONE TEXT," + "DATA_CONSEGNA TEXT,"
				+ "TOKEN_IX TEXT, "
				+ "AOOS_IX TEXT, "
				+ "UOS_IX TEXT, "
				+ "SUBSCRIPTION_IX TEXT, "
				+ "INVOICE_IX TEXT, "
				+ "STATO_SDI TEXT, "
				+ "STATO TEXT " 
				+ ")";

			default:
				break;
			}
			
			  try {
                  DAOFatture.creaIndici(conn);
              } catch (SQLException e) {
                  // mLogger.e(TAG, e.getMessage());
                  // ignore,
                  // table already exists...
              }
			
			
		} catch (Exception e) {
			logger.error(TAG, e);
		}



		// creiamo le tabelle
		try {
			try {
				Statement stat;
				stat = conn.createStatement();
			 
				// stat.executeUpdate("drop table if exists people;");
				stat.executeUpdate(create);


			} catch (SQLException e) {
				// mLogger.e(TAG, e.getMessage());
				// ignore,
				// table already exists...
			}


		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				logger.error(TAG, e);
			}
		}



	}

	public boolean insertFattura(Fattura fattura) {
		Connection connection = null;
		boolean insert = false;
		
		try {
			connection = databaseManager.getConnection(database);
			insert = DAOFatture.insert(connection, fattura);
		} finally {
			silentClose(connection);
		}
		return insert;
	}

	public Fattura findFatturaByNumero(String azienda, String tipo, String tipoDoc, String data, String numero, String piva) {
		Connection connection = null;
		Fattura fattura;
	
		try {
			connection = databaseManager.getConnection(database);
			fattura = DAOFatture.findFatturaByNumero(connection, azienda, tipo, tipoDoc, data, numero, piva);
		} finally {
			silentClose(connection);
		}
		return fattura;
	}



	public boolean deleteFatturaByNumero(String numero) {
		Connection connection = null;
		
		boolean delete = false;
		try {
			connection = databaseManager.getConnection(database);
			delete = DAOFatture.deleteFatturaByNumero(connection, numero);
		} finally {
			silentClose(connection);
		}
		return delete;
	}

	public List<Sdi> readDataSdi() throws SQLException {
		Connection connection = null;
		List<Sdi> sdi;
		Database mssql = Application.getInstance().getMssql();
		try {
			connection = databaseManager.getConnection(mssql);
			sdi = DAOSdi.readDataSdi(connection);
		} finally {
			silentClose(connection);
		}
		return sdi;
	}

	public List<Sdi> readDataSdiActive() throws SQLException {
		Connection connection = null;
		List<Sdi> sdi;
		Database mssql = Application.getInstance().getMssql();
		try {
			connection = databaseManager.getConnection(mssql);
			sdi = DAOSdi.readDataSdiActive(connection);
		} finally {
			silentClose(connection);
		}
		return sdi;
	}

	public Euroquote readEuroquote(String numero, String data, String partitaIva) throws SQLException {
		Connection connection = null;
		Euroquote euroquote;
		Database mssqlEq = Application.getInstance().getMssqlEq();
		try {
			connection = databaseManager.getConnection(mssqlEq);
			euroquote = DAOEuroquote.readProtocollo(connection, numero, data, partitaIva);
		} finally {
			silentClose(connection);
		}
		return euroquote;
	}

	public boolean insertDataAs400(Fattura fattura, String uuid, int row) throws SQLException {
		Connection connection = null;
		boolean insert = false;
		Database as400 = Application.getInstance().getAs400();
		try {
			connection = databaseManager.getConnection(as400);
			insert = DAOAS400.insertDati(connection, uuid, row, fattura);
		} finally {
			silentClose(connection);
		}
		return insert;
	}
 	
	public Registrazione readProtocollo(String idSdi) throws SQLException {
		Connection connection = null;
		Registrazione registrazione = null;
		Database as400 = Application.getInstance().getAs400();
		try {
			connection = databaseManager.getConnection(as400);
			registrazione = DAOAS400.readProtocollo(connection, idSdi);
		}catch (Exception e) {
			logger.error(TAG, e);
		} 

		finally {
			silentClose(connection);
		}
		return registrazione;
	}

	public boolean insertCmdAs400(String uuid, String ccie) throws SQLException {
		Connection connection = null;
		boolean insert = false;
		Database as400 = Application.getInstance().getAs400();
		try {
			connection = databaseManager.getConnection(as400);
			insert = DAOAS400.insertCmd(connection, uuid, ccie);
		} finally {
			silentClose(connection);
		}
		return insert;
	}
	
	public Fornitore readFornitore(String partitaIva, String codiceFiscale) throws SQLException {
		Connection connection = null;
		Fornitore fornitore = null;
		Database as400 = Application.getInstance().getAs400();
		try {
			connection = databaseManager.getConnection(as400);
			fornitore = DAOAS400.readFornitore(connection, partitaIva, codiceFiscale);
		}catch (Exception e) {
			logger.error(TAG, e);
		} 

		finally {
			silentClose(connection);
		}
		return fornitore;
	}


	public static DAO create(DatabaseManager dm){
		DAO dao = new DAO();
		dao.setDatabaseManager(dm);
		return dao;
	}

	public static void silentClose(Connection conn) {
		try {
			if (conn != null)
				conn.close();
		} catch (Exception e) {
			// ignore
		}
	}


}
