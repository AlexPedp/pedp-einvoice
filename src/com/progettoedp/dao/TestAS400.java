package com.progettoedp.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.progettoedp.data.Fattura;
import com.progettoedp.data.Pagamento;
import com.progettoedp.main.Application;
import com.progettoedp.testo.Testo;

public class TestAS400 {
	
	public static void main(String[] args)  {
		
		System.out.println("-------- MySQL JDBC Connection Testing ------------");

		try {
			Class.forName("com.ibm.as400.access.AS400JDBCDriver");
		} catch (ClassNotFoundException e) {

			System.out.println("Where is your AS400 JDBC Driver?");
			e.printStackTrace();
			return;
		}

		System.out.println("AS400 JDBC Driver Registered!");
		Connection connection = null;
		
		
		try {
			String host_as400 = Testo.IP_AS400;
			String user_as400 = Testo.USER_AS400;
			String password_as400 = Testo.PWD_AS400;
			
			connection = DriverManager.getConnection("jdbc:as400://" + host_as400, user_as400, password_as400);
			
			String uuid = UUID.randomUUID().toString().substring(0,8);
			Fattura fattura = new Fattura();
			fattura.setNumero("1");
			fattura.setData("2019/01/01");
			fattura.setTipoDocumento("TD04");
			fattura.setTotaleFattura(10.00);
			fattura.setPartitaIva("03410930261");
			fattura.setRagioneSociale("ECONORMA Sas");
			fattura.setDataRitiro("2017/05/01");
			fattura.setIdSdi("0303093939");
			
			List<Pagamento> pagamenti = new ArrayList<Pagamento>();
			fattura.setPagamenti(pagamenti);
			
		  
		 	DAOAS400.insertDati(connection, uuid, 1, fattura);
		 	DAOAS400.insertCmd(connection, uuid, Testo.CMD_AS400_1);
		 
 			
		} catch (Exception e) {
			System.out.println(e);
		}
	
	}

}
