package com.progettoedp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.progettoedp.data.Sdi;
import com.progettoedp.main.Application;
import com.progettoedp.testo.Testo;
import com.progettoedp.util.Logger;


public class DAOSdi {

	private static final Logger logger = Logger.getLogger(DAOSdi.class);
	private static final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	//	private static final SimpleDateFormat sdfSql = new SimpleDateFormat("yyyy/MM/dd");
	private static final String TAG = DAOSdi.class.getCanonicalName();

	private static final String TABLE = Application.getInstance().getTableName();

	private static final String F_FILE = "FA__FILE";
	private static final String F_CODICE = "TRCODICE";
	private static final String F_ALTRO_CODICE = "CPCODICE";
	private static final String F_DATA_DOC = "FADATDOC";
	private static final String F_NUM_DOC = "FANUMDOC";
	private static final String F_TOT_DOC = "FATOTDOC";
	private static final String F_DATA_RITIRO = "FADATRIT";
	private static final String F_DATA_SDI_1 = "UTDC";
	private static final String F_DATA_SDI_2 = "UTDV";
	private static final String F_ID_SDI = "FAIDESDI";
	private static final String F_DATA_RIC = "FADATEME";
	private static final String F_DATA_CON = "FADAORCO";

	public static List<Sdi> readDataSdi(Connection connection) throws SQLException{

		String databaseName = connection.getCatalog();
		String partitaIva = Application.getInstance().getPartitaIva();

		int days = Application.getInstance().getSubbedDays();

		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.DATE, -7);
		if (days > 0){
			cal.add(Calendar.DATE, -days);
		} else{
			cal.add(Calendar.DATE, -7);
		}

		String dataRitiro = sdf.format(cal.getTime());

		//		String dataRitiro = "2019/03/04";

		try{
			PreparedStatement st =null;

			String select = "SELECT " +
					F_FILE+", "+
					F_CODICE+", " +
					F_DATA_DOC+", " +
					F_NUM_DOC+", " +
					F_TOT_DOC+", " +
					F_DATA_RITIRO+", " +
					F_DATA_SDI_1+", " +
					F_DATA_SDI_2+", " +
					F_ID_SDI +","     +
					F_ALTRO_CODICE    +
					" FROM "+ databaseName + ".dbo." + TABLE + " WHERE " + F_DATA_RITIRO + " >= " + "'" + dataRitiro + "'" +
					" AND " + F_CODICE + " <> " + "'" + partitaIva + "'" +  
					" OR " +  F_DATA_RITIRO + " >= " + "'" + dataRitiro + "'" +
					" AND " + F_CODICE + " = " + "'" + partitaIva + "'" +  
					" AND " + F_ALTRO_CODICE + " <> " + "'" + partitaIva + "'" +  
					" ORDER BY " + F_DATA_RITIRO ; 

			logger.debug(TAG, "Query SDI: " + "|" + select );
			st = connection.prepareStatement(select);

			ResultSet rs = st.executeQuery();
			List<Sdi> sdiArray = new ArrayList<Sdi>();

			while(rs.next()){
				String xmlFile = rs.getString(1);
				String partitaIvaDoc = rs.getString(2);
				java.sql.Date dataDoc = rs.getDate(3);
				String numDoc = rs.getString(4);
				Double totDoc = rs.getDouble(5);
				java.sql.Timestamp dataRit = rs.getTimestamp(6);
				java.sql.Timestamp dataRic1 = rs.getTimestamp(7);
				java.sql.Timestamp dataRic2 = rs.getTimestamp(8);
				String idSdi = rs.getString(9);
				String partitaIvaCli = rs.getString(10);

//								if (!xmlFile.trim().equals("IT00178340261_GTO (1).xml")){
//									continue;
//								}

				if (xmlFile != null && xmlFile.trim().length() > 0) {  

					if ((!partitaIva.trim().equals(partitaIvaDoc.trim())) || (partitaIva.trim().equals(partitaIvaDoc.trim()) && !partitaIva.trim().equals(partitaIvaCli.trim()))) {
						Sdi sdi = new Sdi();
						if (dataRic1 != null)
							sdi.setDataSdi1(sdf.format(dataRic1));
						if (dataRic2 != null)
							sdi.setDataSdi2(sdf.format(dataRic2));
						if (dataRit !=null)
							sdi.setDataRitiro(sdf.format(dataRit));
						sdi.setNomeXml(xmlFile);
						sdi.setNumeroDoc(numDoc);
						sdi.setDataDoc(sdf.format(dataDoc));
						sdi.setPartitaIvaDoc(partitaIvaDoc);
						sdi.setIdSdi(idSdi);
						sdi.setTipo(Testo.PASSIVA);
						sdi.setPartitaIvaCli(partitaIvaCli);
						sdiArray.add(sdi); 
						logger.debug(TAG, "SDI aggiunto: " + "|" +sdi.getNomeXml() + "|" + sdi.getNumeroDoc() + "|" + sdi.getDataDoc() + "|" + partitaIvaDoc +  "|"  + partitaIvaCli);
					}
				}

			}
			rs.close();

			return sdiArray;

		}catch (SQLException e) {
			logger.error(TAG,e);
		}
		return null;
	}

	public static List<Sdi> readDataSdiActive(Connection connection) throws SQLException{

		String databaseName = connection.getCatalog();
		String partitaIva = Application.getInstance().getPartitaIva();

		int days = Application.getInstance().getSubbedDays();

		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.DATE, -7);
		if (days > 0){
			cal.add(Calendar.DATE, -days);
		} else{
			cal.add(Calendar.DATE, -7);
		}

		String dataConsegna = sdf.format(cal.getTime());

		//		String dataRitiro = "2019/03/04";

		try{
			PreparedStatement st =null;

			String select = "SELECT " +
					F_FILE+", "+
					F_CODICE+", " +
					F_DATA_DOC+", " +
					F_NUM_DOC+", " +
					F_TOT_DOC+", " +
					F_DATA_RITIRO+", " +
					F_DATA_SDI_1+", " +
					F_DATA_SDI_2+", " +
					F_DATA_RIC+", " +
					F_DATA_CON +", " +
					F_ID_SDI          +
					" FROM "+ databaseName + ".dbo." + TABLE + " WHERE " + F_DATA_CON + " >= " + "'" + dataConsegna + "'" +
					" AND " + F_CODICE + " = " + "'" + partitaIva + "'" +  
					" AND " + F_ID_SDI + " <> " + "'           '" +  
					" ORDER BY " + F_DATA_CON ; 

			st = connection.prepareStatement(select);

			ResultSet rs = st.executeQuery();
			List<Sdi> sdiArray = new ArrayList<Sdi>();

			while(rs.next()){
				String xmlFile = rs.getString(1);
				String partitaIvaDoc = rs.getString(2);
				java.sql.Date dataDoc = rs.getDate(3);
				String numDoc = rs.getString(4);
				Double totDoc = rs.getDouble(5);
				java.sql.Timestamp dataRit = rs.getTimestamp(6);
				java.sql.Timestamp dataRic1 = rs.getTimestamp(7);
				java.sql.Timestamp dataRic2 = rs.getTimestamp(8);
				java.sql.Timestamp dataRic = rs.getTimestamp(9);
				java.sql.Timestamp dataCon = rs.getTimestamp(10);
				String idSdi = rs.getString(11);

				//				if (!xmlFile.trim().equals("IT00643500267_00BFH (1).xml")){
				//					continue;
				//				}

				if (xmlFile != null && xmlFile.trim().length() > 0 &&  partitaIva.trim().equals(partitaIvaDoc.trim())) {
					Sdi sdi = new Sdi();
					if (dataRic1 != null)
						sdi.setDataSdi1(sdf.format(dataRic1));
					if (dataRic2 != null)
						sdi.setDataSdi2(sdf.format(dataRic2));
					if (dataRit !=null)
						sdi.setDataRitiro(sdf.format(dataRit));
					if (dataCon != null)
						sdi.setDataConsegna(sdf.format(dataCon));
					if (dataRic != null)
						sdi.setDataRicezione(sdf.format(dataRic));
					sdi.setNomeXml(xmlFile);
					sdi.setNumeroDoc(numDoc);
					sdi.setDataDoc(sdf.format(dataDoc));
					sdi.setPartitaIvaDoc(partitaIvaDoc);
					sdi.setIdSdi(idSdi);
					sdi.setTipo(Testo.ATTIVA);
					sdiArray.add(sdi); 
					logger.debug(TAG, "SDI aggiunto: " + "|" +sdi.getNomeXml() + "|" + sdi.getDataDoc());
				}

			}
			rs.close();

			return sdiArray;

		}catch (SQLException e) {
			logger.error(TAG,e);
		}
		return null;
	}


}
