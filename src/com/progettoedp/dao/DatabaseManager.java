package com.progettoedp.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.progettoedp.data.Database;
import com.progettoedp.main.Application;
import com.progettoedp.testo.Testo;
import com.progettoedp.util.Logger;
 

public class DatabaseManager {

	private static DatabaseManager manager;
	private String type;
	private String databaseName;
	private static final Logger logger = Logger.getLogger(DatabaseManager.class);
	private static final String TAG = DatabaseManager.class.getCanonicalName();
	private List<Database> databases = new ArrayList<Database>();
	
	
	public DatabaseManager(List<Database> databases){
		this.databases = databases;
	}

//	public static synchronized DatabaseManager getInstance(String type) {
//		if (manager == null) {
//			manager = new DatabaseManager(type);
//		}
//		return manager;
//	}

	public boolean init() {
		
		
		for (Database db: databases){
			
			try {
				
				switch (db.getType()) {
				case Testo.SQLITE:
					Class.forName("org.sqlite.JDBC");
					break;
				case Testo.FIREBIRD:
					Class.forName("org.firebirdsql.jdbc.FBDriver");
					break;
				case Testo.MSSQL:
					Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
					break;
				case Testo.AS400:	
					Class.forName("com.ibm.as400.access.AS400JDBCDriver");
					break;
				default:
					break;
				}
					
			} catch (ClassNotFoundException e) {
				logger.error(TAG, "non e' stato trovato il driver");
				return false;
			}
		}
	 
		 
		return true;
	}

	public Connection getConnection(Database database) {
		
		String host = database.getHost();
		String databaseName = database.getName();
		String user = database.getUser();
		String password = database.getPassword();
		String instance = database.getInstance();
		
		Connection conn = null;
		try {
			
			switch (database.getType()) {
			case Testo.SQLITE:
				conn = DriverManager.getConnection("jdbc:sqlite:database.db");
				break;
			case Testo.MSSQL:
				
				String url = null;
//				jdbc:sqlserver://srvsem01;instanceName=sqlserver2014
				if (instance != null) {
					 url = "jdbc:sqlserver://" + host + ";" + "instanceName=" + instance + ";DatabaseName=" + databaseName;	
				} else {
					 url = "jdbc:sqlserver://" + host + ";" + "DatabaseName=" + databaseName;
					 
				}
				
			   conn = DriverManager.getConnection(url,user, password);
				break;
			case Testo.AS400:
				 conn = DriverManager.getConnection("jdbc:as400://" + host, user, password);
				break;
			default:
				break;
			}
			
			
		} catch (SQLException e) {
			logger.error(TAG, "non e' stato possibile accedere al db: " + e);
		}
		return conn;
 
	}

}
