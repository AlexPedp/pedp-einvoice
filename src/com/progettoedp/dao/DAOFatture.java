package com.progettoedp.dao;

import java.lang.reflect.Type;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.progettoedp.data.Dettaglio;
import com.progettoedp.data.Fattura;
import com.progettoedp.data.Pagamento;
import com.progettoedp.data.Riepilogo;
import com.progettoedp.testo.Testo;
import com.progettoedp.util.Logger;


public class DAOFatture {

	private static final Logger logger = Logger.getLogger(DAOFatture.class);
	private static final String TAG = DAOFatture.class.getCanonicalName();

	private static final String TABLE = "FATTURE";

	private static final String F_ID = "ID";
	private static final String F_AZIENDA = "AZIENDA";
	private static final String F_ATT_PASS = "ATT_PASS";
	private static final String F_TIPO="TIPO";
	private static final String F_NUMERO="NUMERO";
	private static final String F_DATA="DATA";
	private static final String F_PARTITA_IVA="PARTITA_IVA";
	private static final String F_RAGIONE_SOCIALE="RAGIONE_SOCIALE";
	private static final String F_TOTALE_FATTURA="TOTALE";
	private static final String F_ALLEGATO="TOTALE";
	private static final String F_DATA_RITIRO="DATA_RITIRO";
	private static final String F_DATA_SDI_1="DATA_SDI_1";
	private static final String F_DATA_SDI_2="DATA_SDI_2";
	private static final String F_ID_SDI="ID_SDI";
	private static final String F_NOME_XML="NOME_XML";
	private static final String F_DETTAGLI="DETTAGLI";
	private static final String F_RIEPILOGHI="RIEPILOGHI";
	private static final String F_PAGAMENTI="PAGAMENTI";
	private static final String F_NUM_PRO_EQ="NUMERO_PRO_EQ";
	private static final String F_DATA_PRO_EQ="DATA_PRO_EQ";
	private static final String F_DATA_RIC="DATA_RICEZIONE";
	private static final String F_DATA_CON ="DATA_CONSEGNA";
	private static final String F_TOKEN_IX ="TOKEN_IX";
	private static final String F_AOOS_IX ="AOOS_IX";
	private static final String F_UOS_IX ="UOS_IX";
	private static final String F_SUBSCRIPTION_IX ="SUBSCRIPTION_IX";
	private static final String F_INVOICE_IX ="INVOICE_IX";


	public static Fattura findFatturaByNumero(Connection connection, String azienda, String tipo, String tipDoc, String datDoc, String numDoc, String piva){

		try{
			PreparedStatement st =null;

			String select = "SELECT " +
					F_ID+", "+
					F_AZIENDA+", "+
					F_ATT_PASS+", "+
					F_TIPO+", " +
					F_NUMERO+", " +
					F_DATA  + ", " + 
					F_PARTITA_IVA  + ", " +
					F_RAGIONE_SOCIALE  + ", " +
					F_TOTALE_FATTURA  + ", " +
					F_ALLEGATO  + ", " +
					F_DATA_RITIRO  + ", " +
					F_DATA_SDI_1  + ", " +
					F_DATA_SDI_2  + ", " +
					F_ID_SDI  + ", " +
					F_NOME_XML  + ", " +
					F_NUM_PRO_EQ + ", " +
					F_DATA_PRO_EQ + ", " +
					F_DATA_RIC + ", " +
					F_DATA_CON + ", " +
					F_TOKEN_IX + ", " +
					F_AOOS_IX + ", " +
					F_UOS_IX + ", " +
					F_SUBSCRIPTION_IX + ", " +
					F_INVOICE_IX + ", " +
					F_DETTAGLI  + ", " +
					F_RIEPILOGHI + ", " +
					F_PAGAMENTI + " " +
					" FROM "+TABLE + " WHERE " + F_NUMERO + "='" + numDoc + "'" + 
					" AND "+ F_ATT_PASS + "='" + tipo + "'" +
					" AND "+ F_TIPO + "='" + tipDoc + "'" +
					" AND "+ F_DATA + "='" + datDoc + "'" +
					" AND "+ F_PARTITA_IVA + "='" + piva + "'" +
					" AND "+ F_AZIENDA + "='" + azienda + "'" +
					" ORDER BY " + F_ID; 

			st = connection.prepareStatement(select);

			ResultSet rs = st.executeQuery();

			while(rs.next()){
				Long id = rs.getLong(1);
				String azi = rs.getString(2);
				String att_pass = rs.getString(3);
				String tipoF = rs.getString(4);
				String numero = rs.getString(5);
				String data = rs.getString(6);
				String partitaIva = rs.getString(7);
				String ragioneSociale = rs.getString(8);
				Double totaleFattura = rs.getDouble(9);
				String allegato = rs.getString(10);
				String dataRitiro = rs.getString(11);
				String dataSdi1 = rs.getString(12);
				String dataSdi2 = rs.getString(13);
				String idSdi = rs.getString(14);
				String nomeXml = rs.getString(15);
				String numProEq = rs.getString(16);
				String dataProEq = rs.getString(17);
				String dataRic = rs.getString(18);
				String dataCon = rs.getString(19);
				String tokenIx = rs.getString(20);
				String aoosIx = rs.getString(21);
				String uosIx = rs.getString(22);
				String subscriptionIx = rs.getString(23);
				String invoiceIx = rs.getString(24);
				String dettagliJson = rs.getString(25);
				String riepiloghiJson = rs.getString(26);
				String pagamentiJson = rs.getString(27);

				Type typeDettaglio = new TypeToken<ArrayList<Dettaglio>>(){}.getType();
				List<Dettaglio> dettagli = new Gson().fromJson(dettagliJson, typeDettaglio);

				Type typeRiepilogo = new TypeToken<ArrayList<Riepilogo>>(){}.getType();
				List<Riepilogo> riepiloghi = new Gson().fromJson(riepiloghiJson, typeRiepilogo);

				Type typePagamento = new TypeToken<ArrayList<Pagamento>>(){}.getType();
				List<Pagamento> pagamenti = new Gson().fromJson(pagamentiJson, typePagamento);

				Fattura fattura = new Fattura();
				fattura.setAzienda(azi);
				fattura.setTipo(att_pass);
				fattura.setTipoDocumento(tipoF);
				fattura.setNumero(numero);
				fattura.setData(data);
				fattura.setPartitaIva(partitaIva);
				fattura.setRagioneSociale(ragioneSociale);
				fattura.setTotaleFattura(totaleFattura);
				fattura.setAllegato(allegato);
				fattura.setDataRitiro(dataRitiro);
				fattura.setDataSdi1(dataSdi1);
				fattura.setDataSdi2(dataSdi2);
				fattura.setIdSdi(idSdi);
				fattura.setNomeXml(nomeXml);
				fattura.setNumProEq(numProEq);
				fattura.setDataProEq(dataProEq);
				fattura.setDataRic(dataRic);
				fattura.setDataCon(dataCon);
				fattura.setTokenIx(tokenIx);
				fattura.setAoosIx(aoosIx);
				fattura.setUosIx(uosIx);
				fattura.setSubscriptionIx(subscriptionIx);
				fattura.setInvoiceIx(invoiceIx);
				fattura.setDettagli(dettagli);
				fattura.setRiepiloghi(riepiloghi);
				fattura.setPagamenti(pagamenti);
				return fattura;

			}
			rs.close();

		}catch (SQLException e) {
			logger.error(TAG,e);
		}
		return null;
	}

	public static List<Fattura> findfattureByTipo(Connection connection, String tipoAttPass){

		List<Fattura> fatture = new ArrayList<Fattura>();

		try{
			PreparedStatement st =null;

			String select = "SELECT " +
					F_ID+", "+
					F_ATT_PASS+", "+
					F_TIPO+", " +
					F_NUMERO+", " +
					F_DATA  + ", " + 
					F_PARTITA_IVA  + ", " +
					F_RAGIONE_SOCIALE  + ", " +
					F_TOTALE_FATTURA  + ", " +
					F_ALLEGATO  + ", " +
					F_DATA_RITIRO  + ", " +
					F_DATA_SDI_1  + ", " +
					F_DATA_SDI_2  + ", " +
					F_ID_SDI  + ", " +
					F_NOME_XML  + ", " +
					F_NUM_PRO_EQ + ", " +
					F_DATA_PRO_EQ + ", " +
					F_DATA_RIC + ", " +
					F_DATA_CON + ", " +
					F_TOKEN_IX + ", " +
					F_AOOS_IX + ", " +
					F_UOS_IX + ", " +
					F_SUBSCRIPTION_IX + ", " +
					F_INVOICE_IX + ", " +
					F_DETTAGLI  + ", " +
					F_RIEPILOGHI + ", " +
					F_PAGAMENTI + " " +
					" FROM "+TABLE + " WHERE " + F_ATT_PASS + "='" + tipoAttPass + "'" + 
					" ORDER BY " + F_ID; 

			System.out.println("Select: " + select);

			st = connection.prepareStatement(select);

			ResultSet rs = st.executeQuery();

			while(rs.next()){
				Long id = rs.getLong(1);
				String att_pass = rs.getString(2);
				String tipo = rs.getString(3);
				String numero = rs.getString(4);
				String data = rs.getString(5);
				String partitaIva = rs.getString(6);
				String ragioneSociale = rs.getString(7);
				Double totaleFattura = rs.getDouble(8);
				String allegato = rs.getString(9);
				String dataRitiro = rs.getString(10);
				String dataSdi1 = rs.getString(11);
				String dataSdi2 = rs.getString(12);
				String idSdi = rs.getString(13);
				String nomeXml = rs.getString(14);
				String numProEq = rs.getString(15);
				String dataProEq = rs.getString(16);
				String dataRic = rs.getString(17);
				String dataCon = rs.getString(18);
				String tokenIx = rs.getString(19);
				String aoosIx = rs.getString(20);
				String uosIx = rs.getString(21);
				String subscriptionIx = rs.getString(22);
				String invoiceIx = rs.getString(23);
				String dettagliJson = rs.getString(24);
				String riepiloghiJson = rs.getString(25);
				String pagamentiJson = rs.getString(26);

				Type typeDettaglio = new TypeToken<ArrayList<Dettaglio>>(){}.getType();
				List<Dettaglio> dettagli = new Gson().fromJson(dettagliJson, typeDettaglio);

				Type typeRiepilogo = new TypeToken<ArrayList<Riepilogo>>(){}.getType();
				List<Riepilogo> riepiloghi = new Gson().fromJson(riepiloghiJson, typeRiepilogo);

				Type typePagamento = new TypeToken<ArrayList<Pagamento>>(){}.getType();
				List<Pagamento> pagamenti = new Gson().fromJson(pagamentiJson, typePagamento);

				Fattura fattura = new Fattura();
				fattura.setTipo(att_pass);
				fattura.setTipoDocumento(tipo);
				fattura.setNumero(numero);
				fattura.setData(data);
				fattura.setPartitaIva(partitaIva);
				fattura.setRagioneSociale(ragioneSociale);
				fattura.setTotaleFattura(totaleFattura);
				fattura.setAllegato(allegato);
				fattura.setDataRitiro(dataRitiro);
				fattura.setDataSdi1(dataSdi1);
				fattura.setDataSdi2(dataSdi2);
				fattura.setIdSdi(idSdi);
				fattura.setNomeXml(nomeXml);
				fattura.setNumProEq(numProEq);
				fattura.setDataProEq(dataProEq);
				fattura.setDataRic(dataRic);
				fattura.setDataCon(dataCon);
				fattura.setTokenIx(tokenIx);
				fattura.setAoosIx(aoosIx);
				fattura.setUosIx(uosIx);
				fattura.setSubscriptionIx(subscriptionIx);
				fattura.setInvoiceIx(invoiceIx);
				fattura.setDettagli(dettagli);
				fattura.setRiepiloghi(riepiloghi);
				fattura.setPagamenti(pagamenti);
				fatture.add(fattura);

			}
			rs.close();

			return fatture;

		}catch (SQLException e) {
			logger.error(TAG,e);
		}
		return null;
	}


	public static boolean insert(Connection connection, Fattura testata){

		try{

			String query = "INSERT INTO FATTURE (DATA_CREAZIONE, DATA_VARIAZIONE, AZIENDA, ATT_PASS, TIPO, NUMERO, DATA, PARTITA_IVA, RAGIONE_SOCIALE, TOTALE, ALLEGATO, DETTAGLI, RIEPILOGHI, PAGAMENTI, DATA_RITIRO, DATA_SDI_1, DATA_SDI_2, ID_SDI, NOME_XML, NUMERO_PRO_EQ, DATA_PRO_EQ, DATA_RICEZIONE, DATA_CONSEGNA, TOKEN_IX, AOOS_IX, UOS_IX, SUBSCRIPTION_IX, INVOICE_IX, STATO_SDI, STATO) VALUES (";

			boolean execute=true;

			String ragioneSociale = null;
			if(testata.getRagioneSociale() != null){
				ragioneSociale = testata.getRagioneSociale();
			} else {
				ragioneSociale = testata.getNome() + " " + testata.getCognome();
			}
			String dataProtocollo = null;
			if  (testata.getDataProEq().trim().length() > 20){
				dataProtocollo = testata.getDataProEq().substring(0, 20);
			} else {
				dataProtocollo = testata.getDataProEq();
			}
			
			PreparedStatement st = connection.prepareStatement("INSERT INTO FATTURE (DATA_CREAZIONE, DATA_VARIAZIONE, AZIENDA, ATT_PASS, TIPO, NUMERO, DATA, PARTITA_IVA, RAGIONE_SOCIALE, TOTALE, ALLEGATO, DETTAGLI, RIEPILOGHI, PAGAMENTI, DATA_RITIRO, DATA_SDI_1, DATA_SDI_2, ID_SDI, NOME_XML, NUMERO_PRO_EQ, DATA_PRO_EQ, DATA_RICEZIONE, DATA_CONSEGNA, TOKEN_IX, AOOS_IX, UOS_IX, SUBSCRIPTION_IX, INVOICE_IX, STATO_SDI, STATO) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
	
			logger.debug(TAG, testata.getDataCreazione() +
					"\n" +  testata.getDataVariazione() +
					"\n" +    testata.getAzienda() +
					"\n" +   testata.getTipo() +
					"\n" +   testata.getTipoDocumento() +
					"\n" +   testata.getNumero() +
					"\n" +   testata.getData() +
					"\n" +   testata.getPartitaIva() +
					"\n" +    ragioneSociale +
					"\n" +   testata.getTotaleFattura()+
					"\n" +    testata.getAllegato()+
					"\n" +    testata.getDettagliJson()+
					"\n" +    testata.getRiepiloghiJson()+
					"\n" +   testata.getPagamentiJson()+
					"\n" +   testata.getDataRitiro()+
					"\n" +   testata.getDataSdi1()+
					"\n" +   testata.getDataSdi2()+
					"\n" +   testata.getIdSdi()+
					"\n" +   testata.getNomeXml()+
					"\n" +   testata.getNumProEq()+
					"\n" +   testata.getDataProEq()+
					"\n" +   testata.getDataRic()+
					"\n" +    testata.getDataCon()+
					"\n" +   testata.getTokenIx()+
					"\n" +    testata.getAoosIx()+
					"\n" +   testata.getUosIx()+
					"\n" +   testata.getSubscriptionIx()+
					"\n" +    testata.getInvoiceIx());

			st.setString(1, testata.getDataCreazione());
			st.setString(2, testata.getDataVariazione());
			st.setString(3, testata.getAzienda());
			st.setString(4,testata.getTipo());
			st.setString(5, testata.getTipoDocumento());
			st.setString(6, testata.getNumero());
			st.setString(7,testata.getData());
			st.setString(8,testata.getPartitaIva());
			st.setString(9, ragioneSociale);
			st.setDouble(10, testata.getTotaleFattura());
			st.setString(11, testata.getAllegato());
			st.setString(12, testata.getDettagliJson());
			st.setString(13, testata.getRiepiloghiJson());
			st.setString(14, testata.getPagamentiJson());
			st.setString(15, testata.getDataRitiro());
			st.setString(16, testata.getDataSdi1());
			st.setString(17, testata.getDataSdi2());
			st.setString(18, testata.getIdSdi());
			st.setString(19, testata.getNomeXml());
			st.setString(20, testata.getNumProEq());
			st.setString(21, dataProtocollo);
			st.setString(22, testata.getDataRic());
			st.setString(23, testata.getDataCon());
			st.setString(24, testata.getTokenIx());
			st.setString(25, testata.getAoosIx());
			st.setString(26, testata.getUosIx());
			st.setString(27, testata.getSubscriptionIx());
			st.setString(28, testata.getInvoiceIx());
			st.setString(29, Testo.IMPORTATO);
			st.setString(30, Testo.IMPORTATO);


//		    debugQuery(query, ragioneSociale, testata);

			execute = st.execute();

			return execute;
		}


		catch(SQLException e){
			logger.error(TAG, e);
			return false;

		}

	}	

	public static boolean deleteFatturaByNumero(Connection connection, String numero){

		try{

			boolean execute=true;

			PreparedStatement st = connection.prepareStatement("DELETE FROM FATTURE WHERE NUMERO = ?");
			st.setString(1,numero);
			execute = st.execute();
			return execute;
		}


		catch(SQLException e){
			logger.error(TAG, e);
			return false;

		}

	}	

	public static void debugQuery(String query, String ragioneSociale, Fattura testata){
		StringBuilder sb = new StringBuilder();
		sb.append(query);

		sb.append(testata.getDataCreazione());
		sb.append(",");
		sb.append( testata.getDataVariazione());
		sb.append(",");
		sb.append( testata.getAzienda());
		sb.append(",");
		sb.append(testata.getTipo());
		sb.append(",");
		sb.append( testata.getTipoDocumento());
		sb.append(",");
		sb.append( testata.getNumero());
		sb.append(",");
		sb.append(testata.getData());
		sb.append(",");
		sb.append(testata.getPartitaIva());
		sb.append(",");
		sb.append( ragioneSociale);
		sb.append(",");
		sb.append( testata.getTotaleFattura());
		sb.append(",");
		sb.append( testata.getAllegato());
		sb.append(",");
		sb.append( testata.getDettagliJson());
		sb.append(",");
		sb.append( testata.getRiepiloghiJson());
		sb.append(",");
		sb.append( testata.getPagamentiJson());
		sb.append(",");
		sb.append( testata.getDataRitiro());
		sb.append(",");
		sb.append( testata.getDataSdi1());
		sb.append(",");
		sb.append( testata.getDataSdi2());
		sb.append(",");
		sb.append( testata.getIdSdi());
		sb.append(",");
		sb.append( testata.getNomeXml());
		sb.append(",");
		sb.append( testata.getNumProEq());
		sb.append(",");
		sb.append( testata.getDataProEq());
		sb.append(",");
		sb.append( testata.getDataRic());
		sb.append(",");
		sb.append( testata.getDataCon());
		sb.append(",");
		sb.append( testata.getTokenIx());
		sb.append(",");
		sb.append( testata.getAoosIx());
		sb.append(",");
		sb.append( testata.getUosIx());
		sb.append(",");
		sb.append( testata.getSubscriptionIx());
		sb.append(",");
		sb.append( testata.getInvoiceIx());
		sb.append(",");
		sb.append( Testo.IMPORTATO);
		sb.append(",");
		sb.append( Testo.IMPORTATO);
		sb.append(")");

		logger.info(TAG, sb.toString());

	}

	public static void  creaIndici(Connection connection) throws SQLException{

		boolean execute = false;
		PreparedStatement st=null;

		try {
			st = connection.prepareStatement("CREATE INDEX FATTURE_IDX ON FATTURE (ATT_PASS, TIPO, DATA, NUMERO, PARTITA_IVA);");
			execute = st.execute();	
		} catch (Exception e) {
			 
		}
		

	}


}
