package com.progettoedp.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.progettoedp.data.Fattura;
import com.progettoedp.data.Pagamento;
import com.progettoedp.data.Sdi;
import com.progettoedp.main.Application;
import com.progettoedp.testo.Testo;

public class TestSqliteToAS400 {

	private static final String tipo ="ATTIVA";
	private static Connection sqliteConnection;
	private static Connection as400Connection;

	public static void main(String[] args) {

		try {

			sqliteConnection = getSqliteConnection();
			as400Connection = getAS400Connection();

			List<Fattura> fattureAttive = DAOFatture.findfattureByTipo(sqliteConnection, tipo);

			System.out.println("Inizio import in AS400 fatture attive");
			String uuid = UUID.randomUUID().toString().substring(0,8);
			int row = 0;
			for (Fattura f: fattureAttive){
				row++;
//				if (row > 1){
//					break;
//				}

				System.out.println("Fattura attiva: " + f.getTipo() + "|" +  f.getTipoDocumento() + "|"  + f.getNumero() + "|" +  f.getData());
				DAOAS400.insertDati(as400Connection, uuid, row, f);
			}
			System.out.println("Inizio command in AS400 fatture attive");
			if (!fattureAttive.isEmpty()){
				DAOAS400.insertCmd(as400Connection, uuid, Testo.CMD_AS400_2);
			}
			System.out.println("Fine import in AS400 fatture attive");


		} catch (Exception e) {
			e.printStackTrace();
		}


	}

	public static Connection getSqliteConnection(){

		System.out.println("-------- Sqlite JDBC Connection Testing ------------");

		try {
			Class.forName("org.sqlite.JDBC");
		} catch (ClassNotFoundException e) {

			System.out.println("Where is your SQLITE JDBC Driver?");
			e.printStackTrace();
			return null;
		}

		System.out.println("SQLITE Driver Registered!");
		Connection connection = null;


		try {
			connection =  DriverManager.getConnection("jdbc:sqlite:database.db");
			return connection;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}


	public static Connection getAS400Connection(){

		System.out.println("-------- AS400 JDBC Connection Testing ------------");

		try {
			Class.forName("com.ibm.as400.access.AS400JDBCDriver");
		} catch (ClassNotFoundException e) {

			System.out.println("Where is your AS400 JDBC Driver?");
			e.printStackTrace();
			return null;
		}

		System.out.println("AS400 JDBC Driver Registered!");
		Connection connection = null;


		try {
			String host_as400 = Testo.IP_AS400;
			String user_as400 = Testo.USER_AS400;
			String password_as400 = Testo.PWD_AS400;

			connection = DriverManager.getConnection("jdbc:as400://" + host_as400, user_as400, password_as400);
			return connection;


		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}


	}



}
