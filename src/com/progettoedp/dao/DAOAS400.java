package com.progettoedp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.progettoedp.data.Fattura;
import com.progettoedp.data.Fornitore;
import com.progettoedp.data.Pagamento;
import com.progettoedp.data.Registrazione;
import com.progettoedp.data.Riepilogo;
import com.progettoedp.main.Application;
import com.progettoedp.testo.Testo;
import com.progettoedp.util.Logger;




public class DAOAS400 {

	private static final Logger logger = Logger.getLogger(DAOAS400.class);
	private static final String TAG = "DAOAS400";

	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");	 
	private static final SimpleDateFormat sdfTime = new SimpleDateFormat("HHmmss");	

	private static final SimpleDateFormat sdfDate1 = new SimpleDateFormat("dd/MM/yy");
	private static final SimpleDateFormat sdfDate2 = new SimpleDateFormat("dd/MM/yyyy");

	private static final String LIBRARY = Application.getInstance().getLibrary_as400();
	private static final String LIBRARY_CONT = Application.getInstance().getLibrary_as400_cont();
	// 	private static final String LIBRARY = "IT_SOL_F";

	private static final String TABLE_COMMAND = LIBRARY + ".IECMD00F";
	private static final String ANA_1 = LIBRARY + ".ANACF10F";
	private static final String ANA_2 = LIBRARY + ".ANACF40F";
	private static final String TAB = LIBRARY + ".TBCOM00F";

	private static final String F_CODICE_1 =  "ACODA1";
	private static final String F_CODICE_2 =  "ACODA4";
	private static final String F_RAGIONE_SOCIALE =  "ARAG11";
	private static final String F_RAGIONE_SOCIALE_2 =  "ARAG21";
	private static final String F_SOTTOCONTO_CLIENTE =  "ASTCC1";
	private static final String F_SOTTOCONTO_FORNITORE =  "ASTCF1";
	private static final String F_CLASSE =  "ACCLA4" ;
	private static final String F_DESCRIZIONE =  "TBDESC";
	private static final String F_PARTITA_IVA =  "APAIV1";
	private static final String F_ANNULLA =  "ACANN1";
	private static final String F_DATA_CESSAZIONE =  "ADCES1";
	private static final String F_CODICE_FISCALE =  "ACFIS1";

	private static final String F_TPTB =  "TBTPTB";
	private static final String F_CTB1 =  "TBCTB1";
	private static final String F_CTB2 =  "TBCTB2";
	private static final String F_CTB3 =  "TBCTB3";

	private static final String TAB_CONT = LIBRARY_CONT + ".MX_CO00001";
	private static final String F_NUM_PRO =  "NRPROTIVA";
	private static final String F_REG_IVA =  "CDREG00001";
	private static final String F_DATA_REG =  "DTREG";
	private static final String F_ID_SDI =  "IDSDI";

	private static final int MAX_SIZE = 5;

	public static boolean insertCmd(Connection connection, String uuid, String ccie){

		try{


			boolean execute=true;

			//		      String uuid = UUID.randomU		UID().toString().substring(0,8);	  

			String uten = null;
			if (Application.getInstance().getAzienda().equals(Testo.FRESCOLAT)){
				uten = Testo.USER_AS400_FRE;
			} else {
				uten = Testo.USER_AS400;
			}

			String lib = Testo.LIB_AS400;
			//		      String ccie = Testo.CMD_AS400;
			String ga = Testo.GA_AS400;
			String utec = Testo.USER_ITACA_AS400;
			String empty = "";

			String dataStr = sdf.format(new Date());
			String timeStr = sdfTime.format(new Date());


			PreparedStatement st = connection.prepareStatement("INSERT INTO " + LIBRARY + ".IECMD00F (ICJOBN, ICCCIE, ICUTEN, ICLGAA, ICLOBJ, ICDTAC, ICORAC, ICUTEC, ICFAS1, ICFAS2, CPCCCHK) VALUES (?,?,?,?,?,?,?,?,?,?,?)");

			st.setString(1, uuid.toUpperCase());
			st.setString(2, ccie.trim());
			st.setString(3, uten.trim());
			st.setString(4, ga.trim());
			st.setString(5, lib.trim());
			st.setInt(6, Integer.parseInt(dataStr));
			st.setInt(7, Integer.parseInt(timeStr));
			st.setString(8, utec.trim());
			st.setString(9, empty);
			st.setString(10, empty);
			st.setString(11, empty);

			execute = st.execute();
			logger.info(TAG, "Command AS400 launched: " + uuid);

			return execute;


		}

		catch(SQLException e){
			logger.error(TAG, e);
			return false;
		}
	}

	public static boolean insertDati(Connection connection, String uuid, int row, Fattura fattura){

		try{


			boolean execute=true;

			//		      String uuid = UUID.randomUUID().toString().substring(0,8);	  

			PreparedStatement st = connection.prepareStatement("INSERT INTO " + LIBRARY + ".IEINP00F (IIJOBN, IIJOBR, IITPRK, IIDATI, IIFSTA, CPCCCHK) VALUES (?,?,?,?,?,?)");

			st.setString(1, uuid.toUpperCase());
			st.setInt(2, row);

			if (fattura.getTipo().equals(Testo.PASSIVA)){
				st.setString(3, "ACQ");
				st.setString(4, createRiga(fattura));
			} else if (fattura.getTipo().equals(Testo.ATTIVA)){
				st.setString(3, "DOC");
				st.setString(4, createRigaDoc(fattura)); 
			}

			st.setString(5, "S");
			st.setString(6, "XXXXXXXXXX");

			execute = st.execute();
			logger.info(TAG, "Fattura AS400 executed: " +  fattura.getTipoDocumento() + "|" + fattura.getData() + "|" + fattura.getNumero() +"|" + fattura.getNomeXml());

			return execute;


		}

		catch(SQLException e){
			logger.error(TAG, e);
			return false;
		}
	}

	public static String getValue(String input, int lenght, boolean zeros){

		if (input == null){
			input =  "";
		}

		if  (input.trim().length() > lenght){
			input = input.substring(0, lenght);
		}
		String ouput = null;
		if (!zeros) {
			ouput = String.format("%-" + lenght + "s",input);
		} else {
			ouput = String.format("%" + lenght + "s",input);
			ouput = ouput.replace(" ","0");
		}

		return ouput;
	}

	public static String createRiga(Fattura fattura){

		StringBuilder sb = new StringBuilder();
		String tipo = fattura.getTipoDocumento();
		String numero = fattura.getNumero();
		String data = fattura.getData().substring(0, 10);
		String totale = String.format("%.2f", fattura.getTotaleFattura()).replaceAll(",", "");
		String partitaIva = fattura.getPartitaIva();

		String fornitore = null;
		if (fattura.getRagioneSociale()!= null){
			fornitore = fattura.getRagioneSociale();
		} else if(fattura.getNome() != null && fattura.getCognome() != null){
			fornitore = fattura.getNome() + " " + fattura.getCognome();
		}

		String dataRegistrazione = fattura.getDataRitiro();
		String protocollo = fattura.getIdSdi();
		String nomeXml = fattura.getNomeXml();

		String dataProtocolloEq = null;
		if (fattura.getDataProEq()!= null){
			dataProtocolloEq = fattura.getDataProEq();
		}
		String protocolloEq = null;
		if (fattura.getNumProEq()!= null){
			protocolloEq = fattura.getNumProEq();
		}

		sb.append(getValue(tipo, 4, false));
		sb.append(getValue(numero, 20, false));
		sb.append(getValue(data, 10, false));
		sb.append(getValue(dataRegistrazione, 10, false));
		sb.append(getValue(protocollo.trim(), 20, false));
		sb.append(getValue(dataProtocolloEq, 10, false));
		sb.append(getValue(protocolloEq, 20, false));
		sb.append(getValue(partitaIva, 11, false));
		sb.append(getValue(fornitore.trim(), 50, false));
		sb.append(getValue(totale, 13, true));

		List<Riepilogo> riepiloghi = fattura.getRiepiloghi();
		int size = riepiloghi.size();
		for (Riepilogo r: riepiloghi){
			String imponibile = String.format("%.2f", r.getImponibile()).replaceAll(",", "");
			String imposta = String.format("%.2f", r.getImposta()).replaceAll(",", "");
			String iva = r.getIva().split("\\.")[0];
			String natura = r.getNatura();
			sb.append(getValue(imponibile, 13, true));
			sb.append(getValue(imposta, 11, true));
			sb.append(getValue(iva, 2, true).replaceAll("\\.", ""));
			sb.append(getValue(natura, 2, false).replaceAll("\\.", ""));
		}

		int result = MAX_SIZE-size;
		if (result != 0){
			for (int i = 0; i < result; i++) {
				sb.append(getValue("0", 13, true));
				sb.append(getValue("0", 11, true));
				sb.append(getValue("", 2, true).replaceAll("\\.", ""));
				sb.append(getValue("", 2, false).replaceAll("\\.", ""));
			}
		}


		List<Pagamento> pagamenti = fattura.getPagamenti();
		size = pagamenti.size();
		for (Pagamento p: pagamenti){
			String modalita = p.getModalita();
			String dataScadenza = p.getScadenza();
			String importo = String.format("%.2f", p.getImporto()).replaceAll(",", "");
			sb.append(getValue(modalita, 4, false));
			sb.append(getValue(dataScadenza, 10, true));
			sb.append(getValue(importo, 13, true).replaceAll("\\.", ""));
		}

		result = MAX_SIZE-size;
		if (result != 0){
			for (int i = 0; i < result; i++) {
				sb.append(getValue("0", 4, false));
				sb.append(getValue("0", 10, true));
				sb.append(getValue("", 13, true).replaceAll("\\.", ""));
			}
		}

		sb.append(getValue(nomeXml, 30, false));
		
		
		String contratto = null;
		if (fattura.getIdDocumento()!= null){
			contratto = fattura.getIdDocumento();
		}
		if (fattura.getCodiceCommessa()!= null){
			contratto = contratto + "-" + fattura.getCodiceCommessa();
		}
		 
		sb.append(getValue(contratto, 30, false));
		

		return sb.toString();

	}

	public static String createRigaDoc(Fattura fattura){

		StringBuilder sb = new StringBuilder();
		String tipo = fattura.getTipoDocumento();
		String numero = fattura.getNumero();
		String data = fattura.getData().substring(0, 10);
		String dataRicezione = fattura.getDataRic();
		String dataConsegna = null;
		if (fattura.getDataCon() != null){
			dataConsegna = fattura.getDataCon();
		} else {
			dataConsegna = "";
		}

		String protocollo = fattura.getIdSdi();
		String nomeXml = fattura.getNomeXml();
		String statoSdi = null;
		if (fattura.getStatoSdi() != null){
			statoSdi = fattura.getStatoSdi();
		} else {
			statoSdi = "";
		}

		sb.append(getValue(tipo, 4, false));
		sb.append(getValue(numero, 20, false));
		sb.append(getValue(data, 10, false));
		sb.append(getValue(dataRicezione, 20, false));
		sb.append(getValue(dataConsegna, 20, false));
		sb.append(getValue(protocollo.trim(), 20, false));
		sb.append(getValue(nomeXml.trim(), 70, false));
		sb.append(getValue(statoSdi.trim(), 70, false));
		return sb.toString();

	}

	public static Fornitore readFornitore(Connection connection, String partitaIva, String codiceFiscale){

		try{

			PreparedStatement st =null; 
			String select = null;
 			
			if (codiceFiscale==null || partitaIva.equals(codiceFiscale)){
				select = "SELECT " +
						F_CODICE_1 +", " +
						F_RAGIONE_SOCIALE+", " +
						F_PARTITA_IVA+" " +
						" FROM "+ ANA_1 +
						" WHERE " + F_ANNULLA + "<>" + "'A'" +  
						" AND " + F_PARTITA_IVA + "=" + "'" + partitaIva.trim() + "'";	
			} else {
				select = "SELECT " +
						F_CODICE_1 +", " +
						F_RAGIONE_SOCIALE+", " +
						F_PARTITA_IVA+" " +
						" FROM "+ ANA_1 +
						" WHERE " + F_ANNULLA + "<>" + "'A'" +  
						" AND " + F_PARTITA_IVA + "=" + "'" + partitaIva.trim() + "'" + 
						" AND " + F_CODICE_FISCALE + "=" + "'" + codiceFiscale.trim() + "'";
			}
			
			logger.info(TAG, select);
			
			st = connection.prepareStatement(select);

			ResultSet rs = st.executeQuery();
			Fornitore fornitore = new Fornitore();

			while(rs.next()){
				String codice = rs.getString(1);
				String ragioneSociale = rs.getString(2);
				String iva = rs.getString(3);
//				String classe = rs.getString(4);
//				String descrizione = rs.getString(5);

				fornitore.setCodice(codice);
				fornitore.setPartitaIva(iva);
				fornitore.setRagioneSociale(ragioneSociale);
//				fornitore.setClasse(classe);
//				fornitore.setDescrizione(descrizione);
			}

			rs.close();

			if (fornitore.getRagioneSociale()!= null){
				logger.info(TAG, "Fornitore AS400 trovato: " + fornitore.getCodice() + "|" + fornitore.getRagioneSociale());
			} else {
				logger.info(TAG, "Fornitore AS400 non trovato: " + partitaIva);
			}

			return fornitore;

		}

		catch(SQLException e){
			logger.error(TAG, e);
			return null;
		}
	}

	public static Registrazione readProtocollo(Connection connection, String idSdi){

		try{

			PreparedStatement st =null; 

			String select = "SELECT " +
					F_NUM_PRO +",  " +
					F_REG_IVA +",  " +
					F_DATA_REG +"  " +
					" FROM "+ TAB_CONT +
					" WHERE " + F_ID_SDI + "=" + "'" + idSdi + "'";


			st = connection.prepareStatement(select);

			ResultSet rs = st.executeQuery();
			Registrazione registrazione = new Registrazione();

			while(rs.next()){
				String numeroProtocollo = String.valueOf(rs.getInt(1));
				String registroIva = rs.getString(2);
				String dataRegistrazione = reformatData(sdfDate1, rs.getString(3));
				registrazione.setNumeroProtocollo(numeroProtocollo);
				registrazione.setRegistroIva(registroIva);
				registrazione.setDataRegistrazione(dataRegistrazione);
				logger.debug(TAG, "Debug protocollo: " + registrazione.toString());
			}

			rs.close();

			if (registrazione.getNumeroProtocollo() != null){
				logger.debug(TAG, "Protocollo AS400 trovato: " + registrazione.getNumeroProtocollo());
			} else {
				logger.debug(TAG, "Protocollo AS400 non trovato: " + idSdi);
			}

			return registrazione;

		}

		catch(SQLException e){
			logger.error(TAG, e);
			return null;
		}
	}
 
	public static String reformatData(SimpleDateFormat fromFormat, String originDate){
		String reformatteDate = null;
		try {
			reformatteDate = sdfDate2.format(fromFormat.parse(originDate.trim()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return reformatteDate;
	}

}
