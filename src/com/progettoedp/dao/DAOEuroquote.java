package com.progettoedp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.progettoedp.data.Euroquote;
import com.progettoedp.data.Sdi;
import com.progettoedp.main.Application;
import com.progettoedp.util.Logger;


public class DAOEuroquote {

	private static final Logger logger = Logger.getLogger(DAOEuroquote.class);
	private static final SimpleDateFormat sdfOld = new SimpleDateFormat("yyyy-MM-dd");
	private static final SimpleDateFormat sdfNew = new SimpleDateFormat("dd/MM/yyyy");
	private static final String TAG = DAOEuroquote.class.getCanonicalName();

	private static final String TABLE = Application.getInstance().getTableNameEq();
	private static final String TABLE_CONF = "STARCONFERENTI";

	private static final String F_CONF = "FATCDCONF";
	private static final String F_ANNO = "FATANNO";
	private static final String F_PERIODO = "FATPERIODO";
	private static final String F_NUM_DOC = "FATNDOC";
	private static final String F_DT_DOC = "FATDTDOC";
	private static final String F_NUM_PRO = "FATNPROT";
	private static final String F_DT_PRO = "FATDTPROT";
	
	private static final String F_CONF_CONF = "CNFCDCONF";
	private static final String F_CONF_PIVA = "CNFPIVA";
	private static final String F_CONF_CFIS = "CNFCDFISC";
	private static final String F_CONF_RAGS = "CNFRAG1";
	
	public static Euroquote readProtocollo(Connection connection, String numero, String data, String partitaIva) throws SQLException{
		
		Date newDate = null;
		String dateDocumento = null;
		try {
			newDate = sdfOld.parse(data);
			dateDocumento = sdfNew.format(newDate);
		} catch (Exception e) {
			// TODO: handle exception
		}
	 	
		String databaseName = connection.getCatalog();
	 
		try{
			PreparedStatement st =null;

			String select = "SELECT " +
					F_CONF+", "+
					F_CONF_CONF+", "+
					F_ANNO+", " +
					F_PERIODO+", " +
					F_NUM_DOC+", " +
					F_DT_DOC+", " +
				 	F_NUM_PRO+", " +
					F_DT_PRO+", " +
					F_CONF_RAGS+", " +
					F_CONF_PIVA+", " +
					F_CONF_CFIS          +
					" FROM "+ databaseName + ".dbo." + TABLE + "," + databaseName + ".dbo." + TABLE_CONF + " WHERE " + F_NUM_DOC + " = " + "'" + numero + "'" +
					" AND " + F_DT_DOC + " = " + "'" + dateDocumento + "'" +  
					" AND " + F_CONF_PIVA + " = " + "'" + partitaIva + "'" +  
					" AND " + F_CONF + " = " + F_CONF_CONF;  
 					 

			st = connection.prepareStatement(select);

			ResultSet rs = st.executeQuery();
		    
			Euroquote euroquote = new Euroquote();
			
			while(rs.next()){
				
				String cdConf = rs.getString(1);
				String conf = rs.getString(2);
				int anno = rs.getInt(3);
				int periodo = rs.getInt(4);
				String numDoc = rs.getString(5);
				java.sql.Date dataDoc = rs.getDate(6);
				String numPro = rs.getString(7);
				java.sql.Date dataPro = rs.getDate(8);
				String rags = rs.getString(9);
				String codFisc = rs.getString(10);
				String piva = rs.getString(11);
			   
				euroquote.setCodice(cdConf);
				euroquote.setDataDocumento(sdfNew.format(dataDoc));
				euroquote.setDataProtocollo(sdfNew.format(dataPro));
				euroquote.setProtocollo(numPro);
				euroquote.setNumeroDocumento(numDoc);
				euroquote.setRagioneSociale(rags);
				
				logger.debug(TAG, "Euroquote trovato protocollo: " + "|" +euroquote.getCodice() + "|" +euroquote.getRagioneSociale() + "|"+ euroquote.getProtocollo());
				return euroquote;
				 
				
			}
			rs.close();
			
			return null;
			 
		}catch (SQLException e) {
			logger.error(TAG,e);
		}
		return null;
	}
  
	
}
