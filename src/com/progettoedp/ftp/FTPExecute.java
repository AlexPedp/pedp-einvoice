package com.progettoedp.ftp;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.net.PrintCommandListener;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;

import com.progettoedp.util.Logger;
 

public class FTPExecute {
	
	private static final Logger logger = Logger.getLogger(FTPExecute.class);
	private static final String TAG = "FTPExecute";

	private FTPClient ftp = null;
	private String ip;
	private String user;
	private String pwd;
	private String hostDir;
//	private String hostDir = "/coge/Albiero/FattElettronica/Passive";

	private List<File> files;

	public FTPExecute(String ip, String user, String pwd, String hostDir, List<File> files) throws Exception { 
		this.ip = ip;
		this.user = user;
		this.pwd = pwd;
		this.hostDir = hostDir;
		this.files = files;
	}
	
	public void run() throws Exception{
		
		ftp = new FTPClient();
		ftp.addProtocolCommandListener(new PrintCommandListener(new PrintWriter(System.out)));
		int reply;
		ftp.connect(ip);
		

		reply = ftp.getReplyCode();
		if (!FTPReply.isPositiveCompletion(reply)) {
			ftp.disconnect();
			logger.error(TAG, "Exception in connecting to FTP Server");
		}
		ftp.login(user, pwd);
		ftp.setFileType(FTP.BINARY_FILE_TYPE);
		ftp.enterLocalPassiveMode();
		
		// 1� file//
		String localFileFullName = files.get(0).getAbsolutePath();
		String fileName = files.get(0).getName();
		
		try {
			uploadFile(localFileFullName, fileName, hostDir);
		} catch (Exception e) {
			logger.error(TAG, e);
		}
		
		// 2� file//
		localFileFullName = files.get(1).getAbsolutePath();
		fileName = files.get(1).getName();
		
		try {
			uploadFile(localFileFullName, fileName, hostDir);
		} catch (Exception e) {
			logger.error(TAG, e);
		}
		
		ftp.disconnect();
		 
	}

	public void uploadFile(String localFileFullName, String fileName, String hostDir)
			throws Exception {
		try(InputStream input = new FileInputStream(new File(localFileFullName))){
		this.ftp.storeFile(hostDir + fileName, input);
		}
	}

	public void disconnect() {
		if (this.ftp.isConnected()) {
			try {
				this.ftp.logout();
				this.ftp.disconnect();
			} catch (IOException f) {

			}
		}
	}
 
	public static void main(String[] args) throws Exception {		
		
		List<File> files = new ArrayList<File>();
		files.add(new File("application.log"));
		files.add(new File("FoglioStileAssoSoftware.xsl"));
		String ip = "192.168.1.2";
		String user = "QSECOFR";
		String pwd = "SECOFRPW";
		String hostDir = "/home/ale/";
		FTPExecute fe = new FTPExecute(ip, user, pwd, hostDir, files);
		fe.run();
		
	}

}