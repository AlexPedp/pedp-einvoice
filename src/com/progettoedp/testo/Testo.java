package com.progettoedp.testo;

public class Testo {
	
	public static final String CONFIG_MODE = "CONFIG";
	
	public static final String SQLITE = "SQLITE";
	public static final String MSSQL = "MSSQL";
	public static final String FIREBIRD = "FIREBIRD";
	public static final String MSSQL_EQ = "MSSQL_EQ";
	public static final String AS400 = "AS400";
	public static final String NOME_XML  = "noname.xml";
	public static final String NOTIFICATION  = "NOTIFICATION";
	public static final String INVOICE  = "INVOICE";
	
	public static final String FRESCOLAT  = "FRESCOLAT";
	
	
	public static final String FATEL = "FATEL";
	public static final String IX = "IX";
	public static final String IXFE = "IXFE";
	
	public static final String GET = "GET";
	public static final String POST = "POST";
	public static final String DELETE = "DELETE";
	public static final String PUT = "PUT";
	
	public static final String TYPE_AUTH = "AUTH";
	public static final String TYPE_IDENTIFY = "IDENTIFY";
	public static final String TYPE_REGISTRY = "REGISTRY";
	public static final String TYPE_ABILITATION = "ABILITATION";
	public static final String TYPE_SERVICES = "SERVICES";
	public static final String TYPE_SUBSCRIPTION = "SUBSCRIPTION";
	public static final String TYPE_TRASMISSION = "TRASMISSION";
	public static final String TYPE_DOWNLOAD_ZIP = "DOWNLOAD_ZIP";
	public static final String TYPE_DOWNLOAD_XML = "DOWNLOAD_XML";
	public static final String TYPE_SUBSCRIPTION_DELETE = "SUBSCRIPTION_DELETE";
	public static final String TYPE_MOVE_BOOKMARK = "MOVE_BOOKMARK";
	public static final String TYPE_COMMIT_BOOKMARK = "COMMIT_BOOKMARK";
	
	public static final String ATTIVA = "ATTIVA";
	public static final String PASSIVA = "PASSIVA";
	
	public static final String ricevutaScarto = "RicevutaScarto";
	public static final String datiTrasmissione = "DatiTrasmissione";
	public static final String cedentePrestatore = "CedentePrestatore";
	public static final String cessionarioCommittente = "CessionarioCommittente";
	public static final String datiGeneraliDocumento = "DatiGeneraliDocumento";
	public static final String dettaglioLinee = "DettaglioLinee";
	public static final String datiPagamento = "DatiPagamento";
	public static final String importoPagamento = "ImportoPagamento";
	public static final String dataScadenzaPagamento = "DataScadenzaPagamento";
	public static final String modalitaPagamento = "ModalitaPagamento";
	
	public static final String datiContratto = "DatiContratto";
	public static final String idDocumento = "IdDocumento";
	public static final String codiceCommessaConvenzione = "CodiceCommessaConvenzione";
	
	public static final String nome = "Nome";
	public static final String cognome = "Cognome";
	
	public static final String codiceValore = "CodiceValore";
	public static final String quantita = "Quantita";
	public static final String prezzoUnitario = "PrezzoUnitario";
	public static final String prezzoTotale = "PrezzoTotale";
	public static final String riga = "NumeroLinea";
	
	public static final String identificativoSdi = "IdentificativoSdI";
	public static final String dataOraRicezione = "DataOraRicezione";
	
	public static final String idCodice = "IdCodice";
	public static final String idPaese = "IdPaese";
	public static final String codiceFiscale = "CodiceFiscale";
	public static final String denominazione = "Denominazione";
 
	 
	public static final String numero = "Numero";
	public static final String data = "Data";
	public static final String tipoDocumento = "TipoDocumento";
	public static final String importoTotaleDocumento = "ImportoTotaleDocumento";
	
	public static final String descrizione = "Descrizione";
	public static final String condizioniPagamento = "CondizioniPagamento";
	
	public static final String imponibileImporto = "ImponibileImporto";
	public static final String aliquotaIVA = "AliquotaIVA";
	public static final String esigibilita = "EsigibilitaIVA";
	public static final String imposta = "Imposta";
	public static final String datiRiepilogo = "DatiRiepilogo";
	
	public static final String allegati = "Allegati";
	public static final String attachment = "Attachment";
	
	public static final String IMPORTATO = "IMPORTED";
	
	public static final String IP_AS400 = "192.168.50.1";
	public static final String LIB_ITACA = "IT_SOL_F";
 	public static final String USER_AS400 = "ITACA";
 	public static final String USER_AS400_FRE = "ITACAV2";
	public static final String PWD_AS400 = "ACATI";
	public static final String LIB_AS400 = "IT_V02_O";
	public static final String GA_AS400 = "GA_V01_O";
	public static final String CMD_AS400_1 = "FE0001";
	public static final String CMD_AS400_2 = "FE0002";
	public static final String USER_ITACA_AS400 = "ITACA";
	
	public static final String IX_AUTH_URL = "https://ixapi.arxivar.it/OAuth/api/v2/account/token";
	public static final String IX_DISCOVERY_URL = "https://ixapi.arxivar.it/Discovery/api/v1/discovery/services";
	public static final String IX_SERVICE_URL = "/api/v1/aoos?serviceUID=";
	public static final String IX_REGISTRY_URL = "https://ixapi.arxivar.it/Registry/api/v1/aoos/";
	public static final String IX_ABILITATION_URL ="https://ixapi.arxivar.it/OAuth/api/v2/account/aoos/";
	public static final String IX_DISCOVERY_SERVICES_URL ="https://ixapi.arxivar.it/Discovery/api/v1/discovery/aoos/";
	public static final String IX_MOVE_PASSIVE_BOOKMARK = "https://ixapi.arxivar.it/IxFeApiV3_Reception_Notification/api/v3/reception/notifications/aoos/";
	public static final String IX_MOVE_ACTIVE_BOOKMARK = "https://ixapi.arxivar.it/IxFeApiV3_Transmission_Notification/api/v3/transmission/notifications/aoos/";
	public static final String IX_DELETE_PASSIVE_SUBSCRIPTION= "https://ixapi.arxivar.it/IxFeApiV3_Reception_Notification/api/v3/reception/notifications/aoos/";
	public static final String IX_DELETE_ACTIVE_SUBSCRIPTION= "https://ixapi.arxivar.it/IxFeApiV3_Transmission_Notification/api/v3/transmission/notifications/aoos/";
	
	public static final String IX_PASSIVE_URL = "https://ixapi.arxivar.it/IxFeApiV3_Reception_Notification/api/v3/reception/notifications/aoos/";
	public static final String IX_ACTIVE_URL = "https://ixapi.arxivar.it/IxFeApiV3_Transmission_Notification/api/v3/transmission/notifications/aoos/";
	
	public static final String IX_DOWNLOAD_ACTIVE_URL = "https://ixapi.arxivar.it/IxFeApiV3_Transmission_Download/api/v3/transmission/download/aoos/" ;
	public static final String IX_DOWNLOAD_PASSIVE_URL = "https://ixapi.arxivar.it/IxFeApiV3_Reception_Download/api/v3/reception/download/aoos/";
	
	public static final String IX_UNWRAPPING_PASSIVE_URL = "https://ixapi.arxivar.it/IxFeApiV3_Reception_UnwrapInvoice/api/v1/reception/download/aoos/";
	public static final String IX_UNWRAPPING_ACTIVE_URL = "https://ixapi.arxivar.it/IxFeApiV3_Reception_UnwrapInvoice/api/v1/reception/download/aoos/";
	
	public static final String XML = "XML";
	public static final String PDF = "PDF";
	public static final String ATTACH = "ATTACH";
	public static final String BACKUP = "BACKUP";
	
	public static final String ITACA = "ITACA";
	public static final String EUROQUOTE = "EUROQUOTE";
	
	public static final String TONIOLO = "TONIOLO";
	public static final String TONIOLO_SDM = "TONIOLO_SDM";
	public static final String ALBIERO = "ALBIERO";
	
}
