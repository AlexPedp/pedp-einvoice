package com.progettoedp.parser;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.progettoedp.data.Dettaglio;
import com.progettoedp.data.Fattura;
import com.progettoedp.data.Pagamento;
import com.progettoedp.data.Riepilogo;
import com.progettoedp.data.Tag;
import com.progettoedp.data.TagType;
import com.progettoedp.main.Application;
import com.progettoedp.testo.Testo;
import com.progettoedp.util.Logger;
 


public class XMLParser {

	private static final Logger logger = Logger.getLogger(XMLParser.class);
	private static final String TAG = "XMLParser";

	private File file;
	private DocumentBuilderFactory dbFactory;
	private DocumentBuilder dBuilder;
	private Document doc;
	private LinkedHashMap <String, Tag> nodes = new LinkedHashMap<String, Tag>();
	private Fattura fattura; 
	private Dettaglio dettaglio;
	private Riepilogo riepilogo;
	private Pagamento pagamento;
	private List<Dettaglio> dettagli;
	private List<Riepilogo> riepiloghi;
	private List<Pagamento> pagamenti;
	private String partitaIva;
	private String type;

	public XMLParser(File file, Fattura fattura, String type){
		this.file = file;
		this.type = type;
		this.fattura = fattura;
		init();
	}

	public void init(){
		partitaIva = Application.getInstance().getPartitaIva();
		
		dettagli = new ArrayList<Dettaglio>();
		riepiloghi = new ArrayList<Riepilogo>();
		pagamenti = new ArrayList<Pagamento>();
		
		switch (type) {
		case Testo.NOTIFICATION:
			
		{
			Tag elementNode =  new Tag();
			elementNode.add(Testo.identificativoSdi);
			elementNode.add(Testo.dataOraRicezione);
		 	TagType tagType = new TagType();
			tagType.setRepeated(false);
			tagType.setType(Testo.ricevutaScarto);
			elementNode.setTagType(tagType);
			nodes.put(Testo.ricevutaScarto, elementNode);
		}
		 
			
			break;
		case Testo.INVOICE:

		{
			Tag elementNode =  new Tag();
			elementNode.add(Testo.idCodice);
			TagType tagType = new TagType();
			tagType.setRepeated(false);
			tagType.setType(Testo.datiTrasmissione);
			elementNode.setTagType(tagType);
			nodes.put(Testo.datiTrasmissione, elementNode);
		}
	 
		{
			Tag elementNode =  new Tag();
			elementNode.add(Testo.numero);
			elementNode.add(Testo.data);
			elementNode.add(Testo.tipoDocumento);
			elementNode.add(Testo.importoTotaleDocumento);
			TagType tagType = new TagType();
			tagType.setRepeated(false);
			tagType.setType(Testo.datiGeneraliDocumento);
			elementNode.setTagType(tagType);
			nodes.put(Testo.datiGeneraliDocumento, elementNode);
		}
		
		{
			Tag elementNode =  new Tag();
			elementNode.add(Testo.idDocumento);
			elementNode.add(Testo.codiceCommessaConvenzione);
			TagType tagType = new TagType();
			tagType.setRepeated(false);
			tagType.setType(Testo.datiGeneraliDocumento);
			elementNode.setTagType(tagType);
			nodes.put(Testo.datiContratto, elementNode);
		}


		{
			Tag elementNode =  new Tag();
			elementNode.add(Testo.idCodice);
			elementNode.add(Testo.codiceFiscale);
			elementNode.add(Testo.denominazione);
			elementNode.add(Testo.nome);
			elementNode.add(Testo.cognome);
			TagType tagType = new TagType();
			tagType.setRepeated(false);
			tagType.setType(Testo.cedentePrestatore);
			elementNode.setTagType(tagType);
			nodes.put(Testo.cedentePrestatore, elementNode);
		}

		{
			Tag elementNode =  new Tag();
//		 	elementNode.add(Testo.idCodice);
		 	elementNode.add(Testo.idPaese);
			
			TagType tagType = new TagType();
			tagType.setRepeated(false);
			tagType.setType(Testo.cessionarioCommittente);
			elementNode.setTagType(tagType);
 			nodes.put(Testo.cessionarioCommittente, elementNode);
		}
		
		{
			Tag elementNode =  new Tag();
		 	elementNode.add(Testo.attachment);
			TagType tagType = new TagType();
			tagType.setRepeated(false);
			tagType.setType(Testo.attachment);
			elementNode.setTagType(tagType);
 			nodes.put(Testo.allegati, elementNode);
		}
		
		{
			Tag elementNode =  new Tag();
			elementNode.add(Testo.descrizione);
			elementNode.add(Testo.codiceValore);
			elementNode.add(Testo.quantita);
			elementNode.add(Testo.prezzoTotale);
			elementNode.add(Testo.prezzoUnitario);
			elementNode.add(Testo.aliquotaIVA);
			elementNode.add(Testo.riga);
			TagType tagType = new TagType();
			tagType.setRepeated(true);
			tagType.setType(Testo.dettaglioLinee);
			elementNode.setTagType(tagType);
			nodes.put(Testo.dettaglioLinee, elementNode);
			
		}
		
		{
			Tag elementNode =  new Tag();
			elementNode.add(Testo.imponibileImporto);
			elementNode.add(Testo.imposta);
			elementNode.add(Testo.aliquotaIVA);
			TagType tagType = new TagType();
			tagType.setRepeated(true);
			tagType.setType(Testo.datiRiepilogo);
			elementNode.setTagType(tagType);
			nodes.put(Testo.datiRiepilogo, elementNode);
		}
		
		{
			Tag elementNode =  new Tag();
			elementNode.add(Testo.condizioniPagamento);
			elementNode.add(Testo.modalitaPagamento);
			elementNode.add(Testo.dataScadenzaPagamento);
			elementNode.add(Testo.importoPagamento);
			TagType tagType = new TagType();
			tagType.setRepeated(true);
			tagType.setType(Testo.datiPagamento);
			elementNode.setTagType(tagType);
			nodes.put(Testo.datiPagamento, elementNode);
		}

			break;

		default:
			break;
		}
		
	
		
	}

	public Fattura run(){
		 
		try {
			
//			InputStream inputStream= new FileInputStream(file);
//			Reader reader = new InputStreamReader(inputStream,"UTF-8");
//			InputSource is = new InputSource(reader);
//			is.setEncoding("UTF-8");
 
			dbFactory = DocumentBuilderFactory.newInstance();
			dBuilder = dbFactory.newDocumentBuilder();
 			doc = dBuilder.parse(file);

			doc.getDocumentElement().normalize();

			 
			for(String nodeName : nodes.keySet()){
				Tag tag = nodes.get(nodeName);
				if(tag!=null){
					readNode(nodeName, tag);
				}
			}

		} catch (Exception e) {
			logger.error(TAG, "Exception: " + e); 
		}
		
		logger.debug(TAG, "Parsed Fattura: " + fattura.getPartitaIva() +  "|" + fattura.getTipoDocumento() + "|" + fattura.getData() + "|" + fattura.getNumero() + "|" + file.getName());
		fattura.setDettagli(dettagli);
		fattura.setRiepiloghi(riepiloghi);
		fattura.setPagamenti(pagamenti);
		return fattura;
		
	}

	public void readNode(String nodeName, Tag elementName){

		NodeList nList = doc.getElementsByTagName(nodeName);

		for (int temp = 0; temp < nList.getLength(); temp++) {
			
			TagType tagType = elementName.getTagType();
			
			if (tagType.isRepeated()){
				
				switch (tagType.getType()) {
				case Testo.dettaglioLinee:
					dettaglio = new Dettaglio();
					break;
				case Testo.datiRiepilogo:
					riepilogo = new Riepilogo();
					break;
				case Testo.datiPagamento:
					pagamento = new Pagamento();
					break;
				default:
					break;
				}
				
				
			}

			Node nNode = nList.item(temp);

			logger.debug(TAG, "TAG Lettura node:"  + nodeName);

			if (nNode.getNodeType() == Node.ELEMENT_NODE) {

				Element element = (Element) nNode;

				List<String> elements = elementName.getElements();

				for (String name : elements){

					try {

						String value =  element.getElementsByTagName(name).item(0).getTextContent();
						logger.debug(TAG, "Node: " + nodeName + " Element: " + name + ":" + value);
						updaateTag(element, name, tagType);
						 
					} catch (Exception e) {
						logger.debug(TAG, "Errore:" + " Elemento non trovato: " + name);
					}
					

				}

			} 
			
			if (tagType.isRepeated()){
				
				switch (tagType.getType()) {
				case Testo.dettaglioLinee:
					dettagli.add(dettaglio);
					break;
				case Testo.datiRiepilogo:
					riepiloghi.add(riepilogo);
					break;
				case Testo.datiPagamento:
					pagamenti.add(pagamento);
					break;
				default:
					break;
				}
				
				
			}


		}

	}

 
	public void updaateTag(Element element, String tag, TagType tagtype){

		Node node = element.getElementsByTagName(tag).item(0);
		String value =  element.getElementsByTagName(tag).item(0).getTextContent();

		switch (tag) {
		case Testo.idCodice:
			switch (tagtype.getType()) {
			case Testo.datiTrasmissione:
				if(partitaIva.equals(value)){
					fattura.setTipo(Testo.ATTIVA);	
				} else {
					fattura.setTipo(Testo.PASSIVA);
				}
				fattura.setPartitaIvaMittente(value);
				break;
			case Testo.cedentePrestatore:
				fattura.setPartitaIva(value);
				break;
			case Testo.cessionarioCommittente:
				fattura.setPartitaIvaCliente(value);
				break;
			}
			break;
		case Testo.idPaese:
			fattura.setNazione(value);
			break;
		case Testo.attachment:
			fattura.setAllegato(value);
			break;
		case Testo.codiceFiscale:
			switch (tagtype.getType()) {
			case Testo.datiTrasmissione:
				break;
			case Testo.cedentePrestatore:
				fattura.setCodiceFiscale(value);
				break;
			case Testo.cessionarioCommittente:
				fattura.setCodiceFiscaleCliente(value);
				break;
			}
		case Testo.denominazione:
			fattura.setRagioneSociale(value);
			break;
		case Testo.nome:
			fattura.setNome(value);
			break;
		case Testo.cognome:
			fattura.setCognome(value);
			break;
		case Testo.data:
			fattura.setData(value);
			break; 
		case Testo.numero:
			fattura.setNumero(value);
			break;
		case Testo.tipoDocumento:
			fattura.setTipoDocumento(value);
			break;
		case Testo.idDocumento:
			fattura.setIdDocumento(value);
			break;
		case Testo.codiceCommessaConvenzione:
			fattura.setCodiceCommessa(value);
			break;
		case Testo.importoTotaleDocumento:
			fattura.setTotaleFattura(Double.parseDouble(value));
			break;
		case Testo.descrizione:
			dettaglio.setDescrizione(value);
			break;	
		case Testo.codiceValore:
			dettaglio.setArticolo(value);
			break;	
		case Testo.riga:
			dettaglio.setRiga(Integer.parseInt(value));
			break;	
		case Testo.quantita:
			dettaglio.setQuantita(Double.parseDouble(value));
			break;	
		case Testo.aliquotaIVA:
			switch (tagtype.getType()) {
			case Testo.datiRiepilogo:
				riepilogo.setIva(value);
				break;
			default:
				dettaglio.setIva(value);
				break;
			}
	 		break;	
		case Testo.prezzoUnitario:
			dettaglio.setPrezzoUnitario(Double.parseDouble(value));
			break;	
		case Testo.prezzoTotale:
			dettaglio.setPrezzoTotale(Double.parseDouble(value));
			break;	
		case Testo.modalitaPagamento:
			pagamento.setModalita(value);
		 	break;
		case Testo.importoPagamento:
			pagamento.setImporto(Double.parseDouble(value));
		 	break;	
		case Testo.dataScadenzaPagamento:
			pagamento.setScadenza(value);
		 	break;	
//		case Testo.aliquotaIVA:
//		
		case Testo.imponibileImporto:
			riepilogo.setImponibile(Double.parseDouble(value));
		 	break;	
		case Testo.imposta:
			riepilogo.setImposta(Double.parseDouble(value));
		 	break;	
		case Testo.esigibilita:
			riepilogo.setEsigibilitaIva(value);
		 	break;
		case Testo.identificativoSdi:
			fattura.setIdSdi(value);
			break;
		case Testo.dataOraRicezione:
			fattura.setDataRic(value);
			fattura.setDataSdi1(value);
			fattura.setDataSdi2(value);
			fattura.setDataRitiro(value);
			break;
		default:
			break;
		}

	}

}
