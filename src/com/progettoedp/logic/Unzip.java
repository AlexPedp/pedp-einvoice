package com.progettoedp.logic;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import com.progettoedp.util.Logger;

public class Unzip {
	
	private static final Logger logger = Logger.getLogger(Unzip.class);
	private static final String TAG = "Unzip";
	
	public String run(File dirFile){
		
			List<String> filesExtracted = new ArrayList<String>();
		
	         try(ZipFile file = new ZipFile(dirFile))   {
            FileSystem fileSystem = FileSystems.getDefault();
           
            Enumeration<? extends ZipEntry> entries = file.entries();
             
             
            String uncompressedDirectory = "./unzip/"; 
            if (!new File(uncompressedDirectory).exists()) {
            	Files.createDirectory(fileSystem.getPath(uncompressedDirectory));	
            }
            String uncompressedDirectoryNotify = "./unzip/notification"; 
            if (!new File(uncompressedDirectoryNotify).exists()) {
            	Files.createDirectory(fileSystem.getPath(uncompressedDirectoryNotify));	
            }
            
             
            //Iterate over entries
            while (entries.hasMoreElements()) {
                ZipEntry entry = entries.nextElement(); 
              
                String uncompressedFileName = uncompressedDirectory + entry.getName();
               
                if (entry.isDirectory())   {
                    logger.info(TAG, "Creating Directory:" + uncompressedDirectory + entry.getName());
                    Files.createDirectories(fileSystem.getPath(uncompressedDirectory + entry.getName()));
                }
             
                else  {
                	
                	  if (new File(uncompressedFileName).exists()) {
                       	continue;
                      }
                	
                    InputStream is = file.getInputStream(entry);
                    BufferedInputStream bis = new BufferedInputStream(is);
                  
                    Path uncompressedFilePath = fileSystem.getPath(uncompressedFileName);
                    Files.createFile(uncompressedFilePath);
                    FileOutputStream fileOutput = new FileOutputStream(uncompressedFileName);
                    while (bis.available() > 0)   {
                        fileOutput.write(bis.read());
                    }
                    fileOutput.close();
                  logger.info(TAG, "Written :" + entry.getName());
                  filesExtracted.add(entry.getName());
                }
            }
            if (!filesExtracted.isEmpty()){
            	return filesExtracted.get(0);
            } else {
            	return null;
            }
        }
        catch(IOException e)  {
        	logger.error(TAG, "Exception :" + e);
        	return null;
        }
	}

}
