package com.progettoedp.logic;

import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.progettoedp.data.Authorization;
import com.progettoedp.data.IX;
import com.progettoedp.data.Notification;
import com.progettoedp.net.WebServiceClient;
import com.progettoedp.testo.Testo;
import com.progettoedp.util.Logger;


public class TestRest {

	private static final Logger logger = Logger.getLogger(TestRest.class);
	private static final String TAG = "TestRest";
	
	private String passiveUrl = "https://ixapidemo.arxivar.it/IxFeApiV3_Reception_Notification/api/v3/reception/notifications/aoos/";
	private String activeUrl = "https://ixapidemo.arxivar.it/IxFeApiV3_Transmission_Notification/api/v3/transmission/notifications/aoos/";

	public static void main(String[] args) {
		TestRest main = new TestRest();
		main.run();
	}

	public void run(){

		// FASE 1 - POST - RECUPERO TOKEN
		logger.info(TAG, "********************* FASE 1 ***************************");
		Authorization auth = new Authorization();
		auth.setClient_id("string");
		auth.setClient_secret("string");
		auth.setGrant_type("password");
		auth.setUsername("fabio.stocco@progettoedp.it");
		auth.setPassword("!GWf!K2Y");
		auth.setClient_version("1");

		String json = new Gson().toJson(auth);
		String url = "https://ixapidemo.arxivar.it/OAuth/api/v2/account/token";
		WebServiceClient webService = new WebServiceClient(url, json);
		String token = webService.post(null, Testo.TYPE_AUTH);

		//FASE 2 - GET - DISCOVERY URL SERVICES
		logger.info(TAG, "********************* FASE 2 ***************************");
		url = "https://ixapidemo.arxivar.it/Discovery/api/v1/discovery/services";
		webService = new WebServiceClient(url, token);
		String baseUri = webService.get(Testo.TYPE_AUTH, null);

		//FASE 3 - RICEZIONE AOO - POST
		logger.info(TAG, "********************* FASE 3 ***************************");
		String type = Testo.IXFE;
		url = baseUri + "/api/v1/aoos?serviceUID=IXFE";
		webService = new WebServiceClient(url, type+token);
		String aoos = webService.post(token, Testo.TYPE_IDENTIFY);


		//FASE4 - RICEZIONE UO- POST
		logger.info(TAG, "********************* FASE 4 ***************************");
		url = "https://ixapidemo.arxivar.it/Registry/api/v1/aoos/" + aoos.trim() + "/uos?serviceUID" + type.trim();
		webService = new WebServiceClient(url, type+token+aoos);
		String uos = webService.post(token, Testo.TYPE_REGISTRY);

		//FASE5 - ABILITAZIONE
		logger.info(TAG, "********************* FASE 5 ***************************");
		url = "https://ixapidemo.arxivar.it/OAuth/api/v2/account/aoos/" + aoos.trim() + "/services/" + type.trim() + "/authorization";
		webService = new WebServiceClient(url, type+token+aoos);
		String result = webService.post(token, Testo.TYPE_ABILITATION);

		//FASE6 - ABILITAZIONE
		logger.info(TAG, "********************* FASE 6 ***************************");
		url = "https://ixapidemo.arxivar.it/Discovery/api/v1/discovery/aoos/" + aoos.trim() + "/services/" + type.trim();
		webService = new WebServiceClient(url, token);
		result = webService.get(Testo.TYPE_SERVICES, null);

		//FASE7 - SOTTOSCRIZIONE
		logger.info(TAG, "********************* FASE 7 ***************************");
		String subscribeRequest	 = "{}";
		url = passiveUrl + aoos.trim() + "/uos/" + uos.trim() + "/subscriptions";
		webService = new WebServiceClient(url, subscribeRequest);
		String subscriptionId = webService.post(token, Testo.TYPE_SUBSCRIPTION);

		//FASE8 - RICEZIONE NOTIFICHE
		logger.info(TAG, "********************* FASE 8 ***************************");
		url = passiveUrl + aoos.trim() + "/uos/" + uos.trim() + "/subscriptions/" + subscriptionId + "/notifications?take=10";
		webService = new WebServiceClient(url, token);
		result = webService.get(Testo.TYPE_TRASMISSION, null);

		Type jsonType = new TypeToken<Collection<Notification>>(){}.getType();
		List<Notification> notifications = (List<Notification>)new Gson().fromJson(result, jsonType);

		logger.info(TAG, "********************* FASE 9 ***************************");
		for(Notification notification : notifications) {

			String invoiceId = notification.getInvoiceId();
			String idSdi = notification.getIdSdi();
			String dateSdi = notification.getDateSdi();

			IX ix = new IX();
			//FASE9 - UPLOAD FILE
			logger.info(TAG, "Download XML file invoiceID: " + invoiceId);
			//		String invoiceId = "28e61ee7-bd0c-4b49-9b9e-855c3b094e94";
			String activefileUrl = "https://ixapidemo.arxivar.it/IxFeApiV3_Transmission_Download/api/v3/transmission/download/aoos/" + aoos.trim() + "/uos/" + uos.trim() + "/invoices/" + invoiceId + "/file";
			String activeZipUrl = "https://ixapidemo.arxivar.it/IxFeApiV3_Transmission_Download/api/v3/transmission/download/aoos/" + aoos.trim() + "/uos/" + uos.trim() + "/invoices/" + invoiceId + "/zip";
			String passiveZipUrl = "https://ixapidemo.arxivar.it/IxFeApiV3_Reception_Download/api/v3/reception/download/aoos/" + aoos.trim() + "/uos/" + uos.trim() + "/invoices/" + invoiceId + "/zip";
			webService = new WebServiceClient(passiveZipUrl, token);
			result = webService.get(Testo.TYPE_DOWNLOAD_ZIP, invoiceId);

			String unwrappingPassiveUrl = "https://ixapidemo.arxivar.it/IxFeApiV3_Reception_UnwrapInvoice/api/v1/reception/download/aoos/"+  aoos.trim() + "/uos/" + uos.trim() + "/invoices/" + invoiceId + "/file";;
			webService = new WebServiceClient(unwrappingPassiveUrl, token);
			String nomeXML = webService.get(Testo.TYPE_DOWNLOAD_XML, invoiceId);

			logger.info(TAG, "Invoice id: " + invoiceId + "|" + "idSdi "+ idSdi + "|" + "nome xml " +  result);


		}
		
		//FASE10 - DELETE SUBSCRIPTION
		logger.info(TAG, "********************* FASE 10 ***************************");
		url = "https://ixapidemo.arxivar.it/IxFeApiV3_Transmission_Notification/api/v3/transmission/notifications/aoos/" + aoos.trim() + "/uos/" + uos.trim() + "/subscriptions/" + subscriptionId;
		webService = new WebServiceClient(url, type+token+aoos);
		result = webService.post(token, Testo.TYPE_SUBSCRIPTION_DELETE);

	}

}
