package com.progettoedp.logic;

import java.awt.print.PrinterJob;
import java.io.File;

import javax.print.PrintService;
import javax.print.PrintServiceLookup;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageTree;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.printing.PDFPageable;
import org.apache.pdfbox.printing.PDFPrintable;
import org.apache.pdfbox.printing.Scaling;

import com.progettoedp.util.Logger;
 

public class PrintPdf {
	
	private static final Logger logger = Logger.getLogger(PrintPdf.class);
	private static final String TAG = "PrintPdf";
	
	public void print(File file, String printer) {
		
		logger.info(TAG, "Stampa PDF:" + file.getAbsolutePath());

		try {
			PDDocument document = PDDocument.load(file);

			PDFPageable p = new PDFPageable(document);
			PDFPrintable printable = null;

			PrintService myPrintService = findPrintService(printer);
			PrinterJob job = PrinterJob.getPrinterJob();
			job.setPageable(new PDFPageable(document));
			job.setPrintService(myPrintService);
			
			PDPageTree pages = document.getPages();
			int count = pages.getCount();
			
			for (int i = 1; i <= count; i++) {
				int number = i-1;
				PDPage page = document.getPage(number);
				PDRectangle mediaBox = page.getMediaBox();
				boolean isLandscape = mediaBox.getWidth() > mediaBox.getHeight();
				int rotation = page.getRotation();
				if (rotation == 90 || rotation == 270)
					isLandscape = !isLandscape;
			}

			
//			if (isLandscape && isPageToScale(document))  {
				printable = new PDFPrintable(document,Scaling.SCALE_TO_FIT);
				job.setPageable(p);
				job.setPrintable(printable);
				logger.info(TAG, "Printer set scale to fit");
//			} 
 			
			logger.info(TAG, "Stampa in corso....." );
			job.print();
			
			document.close();
			
		} catch (Exception e) {
			logger.error(TAG, e);
		}


	}

	private static PrintService findPrintService(String printerName) {
		PrintService[] printServices = PrintServiceLookup.lookupPrintServices(null, null);

		for (PrintService printService : printServices) {
//			System.out.println(printService.getName());
			if (printService.getName().trim().equals(printerName)) {
				logger.info(TAG, "Stampante trovata: " + printService.getName().trim());
				return printService;
			}
		}
		logger.info(TAG, "Stampante non trovata: " + printerName);
		return null;
	}

	public static boolean isPageToScale(PDDocument document){
		boolean isPageToScale = true;
//		boolean isPageToScale = false;
//		try {
//			String s =  new PDFTextStripper().getText(document);
//			if (s.contains(CARREFOUR) || (s.contains(AUCHAN))){
//				isPageToScale = true;
//				return isPageToScale;
//			}
//			 
//		} catch (Exception e) {
//			isPageToScale = false;
//			return isPageToScale;
//		}
		return isPageToScale;
	}

}
