package com.progettoedp.logic;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.progettoedp.dao.DAO;
import com.progettoedp.data.Euroquote;
import com.progettoedp.data.Fattura;
import com.progettoedp.data.FileType.TYPE;
import com.progettoedp.data.Fornitore;
import com.progettoedp.data.Sdi;
import com.progettoedp.main.Application;
import com.progettoedp.model.XMLModel;
import com.progettoedp.parser.XMLParser;
import com.progettoedp.testo.Testo;
import com.progettoedp.util.Logger;

public class Import {

	private DAO dao;
	private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	private static final Logger logger = Logger.getLogger(Import.class);
	private static final String TAG = "Import";
	private LinkedHashMap<TYPE, File> appFiles = new LinkedHashMap<TYPE, File>();
	private TYPE type; 
	private String printerName = Application.getInstance().getPrinterName();
	private String secondPrinterName = Application.getInstance().getSecondPrinterName();
	private List<Fattura> fatturePassive = new ArrayList<Fattura>();
	private List<Fattura> fattureAttive = new ArrayList<Fattura>();

	public Import(DAO dao){
		this.dao = dao;
	}

	public void createFiles(Fattura fattura){
		//		String id = fattura.getNazione() + fattura.getPartitaIva() + "_" + fattura.getNumero().replaceAll("/", "_");

		String id = null;
		if (fattura.getRagioneSociale() != null){
			String ragioneSociale =  fattura.getRagioneSociale().replaceAll("\\.", "").replaceAll(" ", "_").replaceAll("\"", "").replaceAll("/", "").replaceAll("\\n", "").replaceAll("\\r", "");
			ragioneSociale =  ragioneSociale.replaceAll("(\\W|^_)*", "");
			id = ragioneSociale + "_" + fattura.getNumero().replaceAll("/", "_");
		} else if (fattura.getNome() != null && fattura.getCognome() != null){
			id = fattura.getNome() + "_" + fattura.getCognome() + "_" + fattura.getNumero().replaceAll("/", "_").replaceAll("\"", "");
		} else {
			id = fattura.getNumero().replaceAll("/", "_");
		}

		addFile(id +".html", TYPE.HTML);
		addFile(id +".pdf", TYPE.PDF);
		addFile(id +"_all.pdf", TYPE.ALL);
		addFile(id +"_allegato.pdf", TYPE.ATTACHMENT);
		addFile("temp_" + id +".html", TYPE.TEMP);
	}

	public void addFile(String name, TYPE type){
		appFiles.put(type, new File(name));
	}

	public void run() throws Exception{

		String xmlBasePath = Application.getInstance().getXmlPath();
		String pdfBasePath = Application.getInstance().getPdfPath();
		String attachBasePath = Application.getInstance().getAttachPath();
		boolean activeInvoice = Application.getInstance().isActiveInvoice();
		String partitaIva = Application.getInstance().getPartitaIva();
		String azienda = Application.getInstance().getAzienda();
	
		try {

			List<Sdi> readDataSdi =  new ArrayList<Sdi>();
 			List<Sdi> readDataSdiActive = new ArrayList<Sdi>();

  			 readDataSdi = dao.readDataSdi();
			
			if (activeInvoice){
				readDataSdiActive = dao.readDataSdiActive();
				readDataSdi.addAll(readDataSdiActive);
			}

			if (readDataSdi.isEmpty())
				logger.error(TAG, "Nessun dato SDI letto");

			for(Sdi s: readDataSdi){
				String nomeXml = s.getNomeXml();
				String dataRitiro = s.getDataRitiro();
				String dataSdi1 = s.getDataSdi1();
				String dataSdi2 = s.getDataSdi2();
				String dataDoc = s.getDataDoc();
				String numDoc = s.getNumeroDoc();
				String partitaIvaDoc = s.getPartitaIvaDoc();
				String partitaIvaCli = s.getPartitaIvaCli();
				String idSdi = s.getIdSdi();
				String dataRic = s.getDataRicezione();
				String dataCon = s.getDataConsegna();
				String tipo = s.getTipo();
				
				Date ritiro = null;
				if (dataSdi2 !=null) {
					ritiro = sdf.parse(dataSdi2);					
				} else if  (dataRitiro !=null){
					ritiro = sdf.parse(dataRitiro);
				}


				Calendar cal = Calendar.getInstance();
				cal.setTime(ritiro);
				String path =  cal.get(Calendar.YEAR) + String.format("%02d", cal.get(Calendar.MONTH)+1);

				File xmlFile = new File(xmlBasePath + "\\" + path + "\\" + nomeXml);

				if (xmlFile.exists()){
					
					Fattura fattura = new Fattura();
					XMLParser xmlParser = new XMLParser(xmlFile, fattura, Testo.INVOICE);
					fattura = xmlParser.run();
					fattura.setDataRitiro(dataRitiro);
					fattura.setDataSdi1(dataSdi1);
					fattura.setDataSdi2(dataSdi2);
					fattura.setDataCon(dataCon);
					fattura.setDataRic(dataRic);
					fattura.setAzienda(azienda);
					if (fattura.getPartitaIva() == null){
						fattura.setPartitaIva(partitaIvaDoc);		
					}
					fattura.setIdSdi(idSdi);
					fattura.setNomeXml(nomeXml);
					
					if (fattura.getTipo().equals(Testo.ATTIVA) && !fattura.getPartitaIva().trim().equals(partitaIva.trim())){
						fattura.setTipo(Testo.PASSIVA);
						logger.info(TAG, "Fattura cambia stato: " + fattura.getPartitaIva() + "|" + fattura.getTipo() +  fattura.getPartitaIva());
					}
					
					createFiles(fattura); 
				 	
					Fattura findFatturaByNumero = dao.findFatturaByNumero(fattura.getAzienda(), fattura.getTipo(), fattura.getTipoDocumento(), fattura.getData(), fattura.getNumero(), fattura.getPartitaIva());
					if (findFatturaByNumero == null) {

						if (activeInvoice && fattura.getTipo().equals(Testo.ATTIVA)){
 							fattureAttive.add(fattura);
							dao.insertFattura(fattura);
						} else {

							Euroquote euroquote = dao.readEuroquote(fattura.getNumero(), fattura.getData(), fattura.getPartitaIva());
							if (euroquote != null) {
								fattura.setNumProEq(euroquote.getProtocollo());
								fattura.setDataProEq(euroquote.getDataProtocollo());
							}  


							fatturePassive.add(fattura);
							dao.insertFattura(fattura);

							Fornitore fornitore = dao.readFornitore(fattura.getPartitaIva(), fattura.getCodiceFiscale());
							if (fornitore!= null){
								if (fornitore.getDescrizione()!= null){
									path = path + "\\" + fornitore.getDescrizione().trim();
								}
							}


							logger.info(TAG, "Fattura inserted: " + fattura.getPartitaIva() +  "|" + fattura.getTipoDocumento() + "|" + fattura.getData() + "|" + fattura.getNumero() + "|" + fattura.getNomeXml());
							XMLModel xmlModel = new XMLModel(xmlFile, fattura, appFiles);
							List<File> files  = xmlModel.run();
							File pdfFile = null;
							File attachFile = null;
							File newPdfFile = null;
							File newAttachFile = null;
							if (files !=null){
								pdfFile = files.get(0);
								newPdfFile = new File(pdfBasePath + "\\" + path + "\\" + pdfFile.getName());
								if(files.size() >= 2){
									attachFile = files.get(1);
									newAttachFile = new File(attachBasePath + "\\" + path + "\\" + attachFile.getName());
								}
								deleteFiles(newPdfFile, newAttachFile);

								if(printerName != null){
									PrintPdf printPdf = new PrintPdf();
									printPdf.print(newPdfFile, printerName);
								}
								if(secondPrinterName != null){
									PrintPdf printPdf = new PrintPdf();
									printPdf.print(newPdfFile, secondPrinterName);
								}
							}
						}
					} else {
						logger.debug(TAG, "Fattura already imported: " +  fattura.getPartitaIva() +  "|" + fattura.getTipoDocumento() + "|" + fattura.getData() + "|" + fattura.getNumero() + "|" + fattura.getNomeXml());
					}


				} else {
					logger.error(TAG, "XML not found: " + xmlFile.toString());
				}

			}

			executeRemote();
	

		} catch (Exception e) {
			logger.error(TAG, "Error: " + e.toString());
		}


	}

	public void runTest() throws Exception{

		String currentDir =  System.getProperty("user.dir");

		//		String xmlBasePath = Application.getInstance().getXmlPath();
		String pdfBasePath = currentDir;
		String attachBasePath = currentDir;
		String azienda = Application.getInstance().getAzienda();

		//		String xmlBasePath = Application.getInstance().getXmlPath();
		//		String pdfBasePath = Application.getInstance().getPdfPath();
		//		String attachBasePath = Application.getInstance().getAttachPath();

		List<Fattura> fatture = new ArrayList<Fattura>();

		File dir = new File(".");
		File finder[] = dir.listFiles(new FileExtensionFilter("xml"));

		for (File file: finder){

			if (file.isDirectory()){
				continue;
			}
			
			Fattura fattura = new Fattura();
			XMLParser xmlParser = new XMLParser(file, fattura, Testo.INVOICE);
			fattura = xmlParser.run();
			fattura.setDataRitiro("08/04/2019");
			fattura.setDataSdi1("08/04/2019");
			fattura.setDataSdi2("08/04/2019");
			fattura.setIdSdi("4545454544");
			fattura.setNomeXml(file.getName());
			fattura.setAzienda(azienda);
			fatture.add(fattura);
			createFiles(fattura);


			Fattura findFatturaByNumero = dao.findFatturaByNumero(fattura.getAzienda(), fattura.getTipo(), fattura.getTipoDocumento(), fattura.getData(), fattura.getNumero(), fattura.getPartitaIva());
			//			if (findFatturaByNumero == null) {
			fatture.add(fattura);
			//				dao.insertFattura(fattura);

			Fornitore fornitore = dao.readFornitore(fattura.getPartitaIva(), fattura.getCodiceFiscale());
			if (fornitore!= null){
				if (fornitore.getDescrizione()!= null){

				}
			}

			logger.info(TAG, "Fattura inserted: " + fattura.getPartitaIva() +  "|" + fattura.getTipoDocumento() + "|" + fattura.getData() + "|" + fattura.getNumero() + "|" + fattura.getNomeXml());
			XMLModel xmlModel = new XMLModel(file, fattura, appFiles);
			List<File> files  = xmlModel.run();
			File pdfFile = null;
			File attachFile = null;
			File newPdfFile = null;
			File newAttachFile = null;
			if (files !=null){
				pdfFile = files.get(0);

				//					PrintPdf printPdf = new PrintPdf();
				//				 	printPdf.print(pdfFile, printerName);

				newPdfFile = new File(pdfBasePath + "/" +  pdfFile.getName());
				if(files.size() >= 2){
					attachFile = files.get(1);
					newAttachFile = new File(attachBasePath + "/"  + attachFile.getName());
				}
				deleteFiles(newPdfFile, newAttachFile);

			}
			//			} else {
			//				logger.info(TAG, "Fattura already imported: " +  fattura.getPartitaIva() +  "|" + fattura.getTipoDocumento() + "|" + fattura.getData() + "|" + fattura.getNumero() + "|" + fattura.getNomeXml());
			//			}
		}

		String uuid = UUID.randomUUID().toString().substring(0,8);
		int row = 0;
		for (Fattura f: fatture){
			row++;
			Application.getInstance().getDao().insertDataAs400(f, uuid, row);
		}
		if (!fatture.isEmpty()){
			Application.getInstance().getDao().insertCmdAs400(uuid, Testo.CMD_AS400_1);
		}

	}

	public void deleteFiles(File renameFilePdf, File renameFileAttach){

		for (Map.Entry<TYPE, File> item : appFiles.entrySet()) {
			TYPE key = item.getKey();
			File value = item.getValue();

			//			System.out.println(value.getAbsolutePath());
			String path = null; 

			if (value.exists()){
				switch (key) {
				case HTML:
					value.delete();
					break;
				case PDF:
					if (!renameFilePdf.getParentFile().exists()){
						renameFilePdf.getParentFile().mkdir();
					}
					value.renameTo(renameFilePdf);
					break;
				case ATTACHMENT:
					if (renameFileAttach != null){
						if (!renameFileAttach.getParentFile().exists()){
							renameFileAttach.getParentFile().mkdir();
						}
						value.renameTo(renameFileAttach);
					}
					break;
				case ALL:
					break;
				case TEMP:
					value.delete();
					break;
				default:
					break;
				}
			}


		}
	}

  
	public boolean indexExists(final List list, final int index) {
		return index >= 0 && index < list.size();
	}
	
	public void executeRemote() {
		
		try {
			
			logger.info(TAG, "Inizio scrittura su AS400 fatture passive Size: " + fatturePassive.size());
			String uuid = UUID.randomUUID().toString().substring(0,8);
			int row = 0;
			for (Fattura f: fatturePassive){
				row++;
				Application.getInstance().getDao().insertDataAs400(f, uuid, row);
			}
			if (!fatturePassive.isEmpty()){
				Application.getInstance().getDao().insertCmdAs400(uuid, Testo.CMD_AS400_1);
			}
			logger.info(TAG, "Fine scrittura su AS400 fatture passive");
			
			
			logger.info(TAG, "Inizio scrittura su AS400 fatture attive Size: " + fattureAttive.size());
			uuid = UUID.randomUUID().toString().substring(0,8);
			row = 0;
			for (Fattura f: fattureAttive){
				row++;
				Application.getInstance().getDao().insertDataAs400(f, uuid, row);
			}
			if (!fattureAttive.isEmpty()){
				Application.getInstance().getDao().insertCmdAs400(uuid, Testo.CMD_AS400_2);
			}
			logger.info(TAG, "Fine scrittura su AS400 fatture attive");
			
		} catch (Exception e) {
			logger.error(TAG, e);
		}
		
		
		
	}

}
