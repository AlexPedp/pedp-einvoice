package com.progettoedp.logic;

import java.io.File;
import java.io.FileInputStream;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.input.BOMInputStream;
import org.apache.commons.lang3.ArrayUtils;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.progettoedp.dao.DAO;
import com.progettoedp.data.Authorization;
import com.progettoedp.data.Fattura;
import com.progettoedp.data.FileType.TYPE;
import com.progettoedp.data.Fornitore;
import com.progettoedp.data.IX;
import com.progettoedp.data.Identifier;
import com.progettoedp.data.Notification;
import com.progettoedp.data.Registrazione;
import com.progettoedp.ftp.FTPExecute;
import com.progettoedp.main.Application;
import com.progettoedp.model.XMLModel;
import com.progettoedp.net.WebServiceClient;
import com.progettoedp.parser.XMLParser;
import com.progettoedp.receipt.RemoteCommand;
import com.progettoedp.receipt.XMLReceipt;
import com.progettoedp.testo.Testo;
import com.progettoedp.util.Logger;

public class ImportIX {

	private String azienda;
	private DAO dao;
	private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	private static final Logger logger = Logger.getLogger(ImportIX.class);
	private static final String TAG = "Import";
	private LinkedHashMap<TYPE, File> appFiles = new LinkedHashMap<TYPE, File>();
	private TYPE type; 
	private String printerName = Application.getInstance().getPrinterName();
	private String secondPrinterName = Application.getInstance().getSecondPrinterName();
	private List<Fattura> fatturePassive = new ArrayList<Fattura>();
	private List<Fattura> fattureAttive = new ArrayList<Fattura>();
	private List<IX> fattureIX = new ArrayList<IX>();
	private String token;
	private String aoos;
	private String uos;
	private String subscriptionId;
	private String partitaIva;
 	private final SimpleDateFormat sdfSubscription = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
 	private final String ipRegex = "(..)\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}";
	
	public ImportIX(String azienda, DAO dao){
		this.azienda=azienda;
		this.dao = dao;
	}

	public void createFiles(Fattura fattura){
		
		String id = null;
		if (fattura.getRagioneSociale() != null){
			String ragioneSociale =  fattura.getRagioneSociale().replaceAll("\\.", "").replaceAll(" ", "_").replaceAll("\\\\", "").replaceAll("/", "").replaceAll("\\n", "").replaceAll("\\r", "").replaceAll("\\*", "");
			ragioneSociale =  ragioneSociale.replaceAll("(\\W|^_)*", "");
			id = ragioneSociale + "_" + fattura.getNumero().replaceAll("/", "_").replaceAll("\\\\", "_").replaceAll("\\*", "");
		} else if (fattura.getNome() != null && fattura.getCognome() != null){
			id = fattura.getNome() + "_" + fattura.getCognome() + "_" + fattura.getNumero().replaceAll("/", "_").replaceAll("\\\\", "_").replaceAll("\\*", "");
		} else {
			id = fattura.getNumero().replaceAll("/", "_").replaceAll("\\\\", "_");
		}

		addFile(id +".html", TYPE.HTML);
		addFile(id +".pdf", TYPE.PDF);
		addFile(id +"_all.pdf", TYPE.ALL);
		addFile(id +"_allegato.pdf", TYPE.ATTACHMENT);
		addFile("temp_" + id +".html", TYPE.TEMP);
	}

	public void addFile(String name, TYPE type){
		appFiles.put(type, new File(name));
	}

	public void run() throws Exception{

		String pdfBasePath = Application.getInstance().getPdfPath();
		String attachBasePath = Application.getInstance().getAttachPath();
		String backupBasePath = Application.getInstance().getBackupPath();
		String xmlBasePath = Application.getInstance().getXmlPath();
		String azienda = Application.getInstance().getAzienda();
		partitaIva = Application.getInstance().getPartitaIva();
		String xmlAs400Path = Application.getInstance().getXmlAs400Path();
		String tipoProtocollo = Application.getInstance().getTipoProtocollo();
		
		boolean ftp_as400 = Application.getInstance().isFtp_as400();
		
		String host = Application.getInstance().getHost_as400();
		String uid = Application.getInstance().getUser_as400();
		String pwd = Application.getInstance().getPassword_as400();
		String libCont = Application.getInstance().getLibrary_as400_cont();
		
		String[] filteredDocuments = Application.getInstance().getFilteredDocuments();

		getXmlFiles(Testo.PASSIVA);

		if (fattureIX.isEmpty())
			logger.error(TAG, "Nessun dato IX letto");
		
		int size = fattureIX.size();
		int read = 0;
		logger.info(TAG, "Start reading invoice: " + size + " please wait..............................");
		
		for(IX ix: fattureIX){
			
			read++;

			String nomeXml = ix.getNomeXml();
			String xmlFilePath = xmlBasePath + nomeXml;
			String backupFilePath = backupBasePath + nomeXml;
		
			File xmlFile = new File(xmlBasePath + nomeXml);
			File backupXmlFile = new File(backupBasePath + nomeXml);
			File xmlAs400File = new File(xmlAs400Path + nomeXml);
		 
			if (xmlFile.exists()){
				
				removeBOM(xmlFile);
				
				Fattura fattura = new Fattura();
				XMLParser xmlParser = new XMLParser(xmlFile, fattura, Testo.INVOICE);
				fattura = xmlParser.run();
				fattura.setDataRitiro(ix.getDateSdi());
				fattura.setDataSdi1(ix.getDateSdi());
				fattura.setDataSdi2(ix.getDateSdi());
				fattura.setIdSdi(ix.getIdSdi());
				fattura.setAoosIx(ix.getAoos());
				fattura.setUosIx(ix.getUos());
				fattura.setTokenIx(ix.getToken());
				fattura.setSubscriptionIx(ix.getSubscription());
				fattura.setInvoiceIx(ix.getInvoice());
				fattura.setNomeXml(xmlFile.getName());
				fattura.setAzienda(azienda);
				fattura.setDataCreazione(sdf.format(new Date()));
				fattura.setDataVariazione(sdf.format(new Date()));

				if (ArrayUtils.contains(filteredDocuments, fattura.getTipoDocumento())) {
					logger.info(TAG, "Fattura SDI scartata: " +  fattura.getPartitaIva() +  "|" + fattura.getTipoDocumento() + "|" + fattura.getData() + "|" + fattura.getNumero() + "|" + nomeXml);
					continue;
				}
				
				// notification XML parser solo per fatture attive
//				if (fattura.getTipo().equals(Testo.ATTIVA)){
//					String notificationFile = "./unzip/notification/" + "IT02355260981_01P1X_NS_001.xml";
//					XMLParser newParsers = new XMLParser(new File(notificationFile), fattura, Testo.NOTIFICATION);
//					fattura = newParsers.run();	
//				}
				
				
				createFiles(fattura);

				Fattura findFatturaByNumero = dao.findFatturaByNumero(fattura.getAzienda(), fattura.getTipo(), fattura.getTipoDocumento(), fattura.getData(), fattura.getNumero(), fattura.getPartitaIva());
				if (findFatturaByNumero == null) {
					
					logger.info(TAG, "Ricevura XML path AS400: " +  xmlAs400Path);
					
					if (xmlAs400Path != null){
						XMLReceipt receipt = new XMLReceipt(xmlFile, fattura);
						File receiptFile = receipt.run();
						
						File receiptAs400File = new File(xmlAs400Path + receiptFile);
						File receiptFileComplete = new File(xmlBasePath + receiptFile);
						logger.info(TAG, "XML file: " + xmlFile + " Receipt File: " + receiptFile);
						
						
						Fornitore fornitore = dao.readFornitore(fattura.getPartitaIva(), fattura.getCodiceFiscale());
						if (fornitore!= null){
							if (fornitore.getRagioneSociale()!= null){
								fattura.setCodice(fornitore.getCodice());
								fattura.setRagioneSocialeGestionale(fornitore.getRagioneSociale());
							}
						}
						
						try {
							
							logger.debug(TAG, "FTP AS400 active: " + ftp_as400);
							if (ftp_as400){
								List<File> ftpFiles = new ArrayList<File>();
								ftpFiles.add(xmlFile);
								ftpFiles.add(receiptFile);
								
								String hostdir = null;
								try {
									hostdir = xmlAs400Path.replaceFirst(ipRegex, "").replaceAll("\\\\",  "/");
									logger.debug(TAG, "HostDir: " + hostdir);
								} catch (Exception e) {
									logger.error(TAG, e);
								}
								FTPExecute ftp = new FTPExecute(host, uid, pwd, hostdir, ftpFiles);
								ftp.run();
							} else {
								
								FileUtils.copyFileToDirectory(xmlFile, new File(xmlAs400Path));
								FileUtils.copyFileToDirectory(receiptFile, new File(xmlAs400Path));
							}
							
//							logger.info(TAG, "Receipt file Path: " + receiptFile.getAbsolutePath() + " XML file path: " + xmlFile.getAbsolutePath());
//							Files.copy(Paths.get(receiptFile.getAbsolutePath()), Paths.get(receiptAs400File.getAbsolutePath()), StandardCopyOption.REPLACE_EXISTING);
//							Files.copy(Paths.get(xmlFile.getAbsolutePath()), Paths.get(xmlAs400File.getAbsolutePath()), StandardCopyOption.REPLACE_EXISTING); 
							
							logger.info(TAG, "Ricevura XML x contabilita generate: " + receiptFile + " in " + xmlAs400Path);
							File backupReceiptFile = new File(backupBasePath + receiptFile.getName());
							receiptFile.renameTo(backupReceiptFile);
							receiptFile.delete();
						} catch(Exception e) {
							logger.error(TAG, "Errore in creazione ricevuta XML: " + e);
						}

						try {
							if (libCont.trim().length()>0){
								RemoteCommand remoteCommand = new RemoteCommand(azienda, host, uid, pwd);
								remoteCommand.run();	
							}
				
						} catch (Exception e) {
							logger.error(TAG, "Errore in remote command AS400: " + e);
						}

					}

					if (tipoProtocollo.equals(Testo.ITACA)){
						Registrazione registrazione = dao.readProtocollo(fattura.getIdSdi());
						if (registrazione!= null){
							if (registrazione.getNumeroProtocollo() != null){

								String dataAccettazione = null;
								if (fattura.getDataRitiro()!= null){
									dataAccettazione = fattura.getDataRitiro();
								} else {
									dataAccettazione = fattura.getDataSdi2();
								}

								fattura.setNumProEq(registrazione.getNumeroProtocollo());
								fattura.setDataProEq(dataAccettazione);
								fattura.setRegistroIva(registrazione.getRegistroIva());
								fattura.setDataRegistrazione(registrazione.getDataRegistrazione());
								
								logger.info(TAG, "Numero protocollo recuperato correttamente: " + fattura.getNumProEq() + "|" + fattura.getDataProEq());

							}
						}	
					}
				 	
					if (fattura.getTipo().equals(Testo.ATTIVA)){
						fattureAttive.add(fattura);
					} else {
						fatturePassive.add(fattura);	
					}
					
					dao.insertFattura(fattura);
		
					logger.info(TAG, "Fattura inserted: " +  read + "|" + size + "|" + fattura.getPartitaIva() +  "|" + fattura.getTipoDocumento() + "|" + fattura.getData() + "|" + fattura.getNumero() + "|" + fattura.getNomeXml());
					XMLModel xmlModel = new XMLModel(xmlFile, fattura, appFiles);
					List<File> files  = xmlModel.run();
					File pdfFile = null;
					File attachFile = null;
					File newPdfFile = null;
					File newAttachFile = null;
					if (files !=null){
						pdfFile = files.get(0);

						PrintPdf printPdf = new PrintPdf();
						if (printerName.trim().length()>0) {
							printPdf.print(pdfFile, printerName);
							logger.info(TAG, "Stampa PDF in stampante: " + printerName);
						}
						if (secondPrinterName.trim().length()>0) {
							printPdf.print(pdfFile, secondPrinterName);
							logger.info(TAG, "Stampa PDF in stampante: " + secondPrinterName);
						}
					
						newPdfFile = new File(pdfBasePath + "/" +  pdfFile.getName());
						if(files.size() >= 2){
							attachFile = files.get(1);
							newAttachFile = new File(attachBasePath + "/"  + attachFile.getName());
						}
						deleteFiles(newPdfFile, newAttachFile);

					}
				} else {
					logger.info(TAG, "Fattura already imported: " +  read + "|" + size + "|" + fattura.getPartitaIva() +  "|" + fattura.getTipoDocumento() + "|" + fattura.getData() + "|" + fattura.getNumero() + "|" + fattura.getNomeXml());
				}
				
				//sposto XML in backup
				try {
					
//					 Path move = Files.move
//						        (Paths.get(xmlFilePath),  
//						        Paths.get(backupFilePath));
					 
					 xmlFile.renameTo(backupXmlFile);
					 xmlFile.delete();
					 
				} catch (Exception e) {
					logger.error(TAG, e); 
				}
				
			} else {
				logger.info(TAG, "XML not found: " +  read + "|" + size + "|"  + xmlFile.toString());
			}

		}
		
		logger.info(TAG, "End reading invoice: please wait..............................");

		executeRemote();

	}
	
	public void runTest() throws Exception{

		String currentDir =  System.getProperty("user.dir");

		String pdfBasePath = Application.getInstance().getPdfPath();
		String attachBasePath = Application.getInstance().getAttachPath();
		String backupBasePath = Application.getInstance().getBackupPath();
		String xmlBasePath = Application.getInstance().getXmlPath();
		String azienda = Application.getInstance().getAzienda();

		File dir = new File(currentDir + "/xml");
		File finder[] = dir.listFiles(new FileExtensionFilter("xml"));

		for (File file: finder){

			if (file.isDirectory()){
				continue;
			}

			String nomeXml = file.getName();
			String aoos = "test";
			String uos = "test";
			String subscription = "test";
			String invoice = "test";

			String xmlFilePath = xmlBasePath + nomeXml;
			String backupFilePath = backupBasePath + nomeXml;

//			File xmlFile = new File("./xml/" + nomeXml);
			File xmlFile = new File(xmlBasePath + nomeXml);
		 

			if (xmlFile.exists()){
				
				Fattura fattura = new Fattura();
				XMLParser xmlParser = new XMLParser(xmlFile, fattura, Testo.INVOICE);
				fattura = xmlParser.run();
				fattura.setDataRitiro("01/01/2019");
				fattura.setDataSdi1("01/01/2019");
				fattura.setDataSdi2("01/01/2019");
				fattura.setIdSdi("111111111");
				fattura.setAoosIx("ddd");
				fattura.setUosIx("ss");
				fattura.setTokenIx("dada");
				fattura.setSubscriptionIx("dada");
				fattura.setInvoiceIx("dd");
				fattura.setNomeXml(xmlFile.getName());
				fattura.setAzienda(azienda);
				fattura.setDataCreazione(sdf.format(new Date()));
				fattura.setDataVariazione(sdf.format(new Date()));
				
				// notification XML parser solo per fatture attive
				if (fattura.getTipo().equals(Testo.ATTIVA)){
					String notificationFile = "./unzip/notification/" + "IT02355260981_01P1X_NS_001.xml";
					XMLParser newParsers = new XMLParser(new File(notificationFile), fattura, Testo.NOTIFICATION);
					fattura = newParsers.run();	
				}
				
				
				createFiles(fattura);

				Fattura findFatturaByNumero = dao.findFatturaByNumero(fattura.getAzienda(), fattura.getTipo(), fattura.getTipoDocumento(), fattura.getData(), fattura.getNumero(), fattura.getPartitaIva());
				if (findFatturaByNumero == null) {
					
					if (fattura.getTipo().equals(Testo.ATTIVA)){
						fattureAttive.add(fattura);
					} else {
						fatturePassive.add(fattura);	
					}
					
					dao.insertFattura(fattura);
				
					//				Fornitore fornitore = dao.readFornitore(fattura.getPartitaIva());
					//				if (fornitore!= null){
					//					if (fornitore.getDescrizione()!= null){
					//
					//					}
					//				}

					logger.info(TAG, "Fattura inserted: " + fattura.getPartitaIva() +  "|" + fattura.getTipoDocumento() + "|" + fattura.getData() + "|" + fattura.getNumero() + "|" + fattura.getNomeXml());
					XMLModel xmlModel = new XMLModel(xmlFile, fattura, appFiles);
					List<File> files  = xmlModel.run();
					File pdfFile = null;
					File attachFile = null;
					File newPdfFile = null;
					File newAttachFile = null;
					if (files !=null){
						pdfFile = files.get(0);

						//					PrintPdf printPdf = new PrintPdf();
						//				 	printPdf.print(pdfFile, printerName);

						newPdfFile = new File(pdfBasePath + "/" +  pdfFile.getName());
						if(files.size() >= 2){
							attachFile = files.get(1);
							newAttachFile = new File(attachBasePath + "/"  + attachFile.getName());
						}
						deleteFiles(newPdfFile, newAttachFile);

					}
				} else {
					logger.debug(TAG, "Fattura already imported: " +  fattura.getPartitaIva() +  "|" + fattura.getTipoDocumento() + "|" + fattura.getData() + "|" + fattura.getNumero() + "|" + fattura.getNomeXml());
				}
				
				 
				
				try {
					 Path move = Files.move 
						        (Paths.get(xmlFilePath),  
						        Paths.get(backupFilePath)); 
				} catch (Exception e) {
					logger.error(TAG, e);
				}
				
			}

		}

		//			executeRemote();

	}


	public void deleteFiles(File renameFilePdf, File renameFileAttach){
		
		String currentDir =  System.getProperty("user.dir");

		for (Map.Entry<TYPE, File> item : appFiles.entrySet()) {
			TYPE key = item.getKey();
			File value = item.getValue();

			String path = null; 

			if (value.exists()){
				switch (key) {
				case HTML:
					value.delete();
					break;
				case PDF:
					if (!renameFilePdf.getParentFile().exists()){
						renameFilePdf.getParentFile().mkdir();
					}
					File oldPdf = new File(currentDir + "\\" + value.getName());
					oldPdf.renameTo(renameFilePdf);
					logger.info(TAG, "Rename PDF: From " + oldPdf + " to " + renameFilePdf);
					if (oldPdf.exists()) {
						oldPdf.delete();	
					}
					break;
				case ATTACHMENT:
					if (renameFileAttach != null){
						if (!renameFileAttach.getParentFile().exists()){
							renameFileAttach.getParentFile().mkdir();
						}
						value.renameTo(renameFileAttach);
						logger.info(TAG, "Rename Attchment PDF From " + value + " to " + renameFileAttach);
					}
					break;
				case ALL:
					break;
				case TEMP:
					value.delete();
					break;
				default:
					break;
				}
			}


		}
	}


	public boolean indexExists(final List list, final int index) {
		return index >= 0 && index < list.size();
	}

	public void executeRemote() {

		try {
			if (!fatturePassive.isEmpty()){

				logger.info(TAG, "Inizio scrittura su AS400 fatture passive Size: " + fatturePassive.size());
				String uuid = UUID.randomUUID().toString().substring(0,8);
				int row = 0;
				for (Fattura f: fatturePassive){
					row++;
					Application.getInstance().getDao().insertDataAs400(f, uuid, row);
				}
				if (!fatturePassive.isEmpty()){
					Application.getInstance().getDao().insertCmdAs400(uuid, Testo.CMD_AS400_1);
				}
				logger.info(TAG, "Fine scrittura su AS400 fatture passive");
			}

			if (!fattureAttive.isEmpty()){

				logger.info(TAG, "Inizio scrittura su AS400 fatture attive Size: " + fattureAttive.size());
				String uuid = UUID.randomUUID().toString().substring(0,8);
				int row = 0;
				for (Fattura f: fattureAttive){
					row++;
					Application.getInstance().getDao().insertDataAs400(f, uuid, row);
				}
				if (!fattureAttive.isEmpty()){
					Application.getInstance().getDao().insertCmdAs400(uuid, Testo.CMD_AS400_2);
				}
				logger.info(TAG, "Fine scrittura su AS400 fatture attive");
			}


		} catch (Exception e) {
			logger.error(TAG, e);
		}

	}

	public void getXmlFiles(String attPass) {
 
		// FASE 1 - POST - RECUPERO TOKEN
		logger.debug(TAG, "********************* FASE 1 ***************************");
		Authorization auth = new Authorization();
		auth.setClient_id(Application.getInstance().getIxClientId());
		auth.setClient_secret(Application.getInstance().getIxClientSecret());
		auth.setGrant_type("password");
		auth.setUsername(Application.getInstance().getIxUserName());
		auth.setPassword(Application.getInstance().getIxPassword());
		auth.setClient_version("1");

		String json = new Gson().toJson(auth);
		String url = Testo.IX_AUTH_URL;
		WebServiceClient webService = new WebServiceClient(url, json);
		token = webService.post(null, Testo.TYPE_AUTH);

		//FASE 2 - GET - DISCOVERY URL SERVICES
		logger.debug(TAG, "********************* FASE 2 ***************************");
		url = Testo.IX_DISCOVERY_URL;
		webService = new WebServiceClient(url, token);
		String baseUri = webService.get(Testo.TYPE_AUTH, null);

		//FASE 3 - RICEZIONE AOO - POST
		logger.debug(TAG, "********************* FASE 3 ***************************");
		String type = Testo.IXFE;
		url = baseUri + Testo.IX_SERVICE_URL + type;
		webService = new WebServiceClient(url, type+token);
		String result = webService.post(token, Testo.TYPE_IDENTIFY);

		Type jsonType = new TypeToken<Collection<Identifier>>(){}.getType();
		List<Identifier> identifiers = (List<Identifier>)new Gson().fromJson(result, jsonType);
		
		aoos = null;
		
		for (Identifier i: identifiers){
			String iva = i.getPartitaIva();
			if (iva.equals(partitaIva)){
				aoos =  i.getIdentifier();
				break;
			}
		}
		
		if (aoos == null){
			logger.error(TAG, "aoos not found");
			return;
		}
		
		//FASE4 - RICEZIONE UO- POST
		logger.debug(TAG, "********************* FASE 4 ***************************");
		url = Testo.IX_REGISTRY_URL + aoos.trim() + "/uos?serviceUID" + type.trim();
		webService = new WebServiceClient(url, type+token+aoos);
		uos = webService.post(token, Testo.TYPE_REGISTRY);

		//FASE5 - ABILITAZIONE
		logger.debug(TAG, "********************* FASE 5 ***************************");
		url = Testo.IX_ABILITATION_URL + aoos.trim() + "/services/" + type.trim() + "/authorization";
		webService = new WebServiceClient(url, type+token+aoos);
		result = webService.post(token, Testo.TYPE_ABILITATION);

		//FASE6 - ABILITAZIONE
		logger.debug(TAG, "********************* FASE 6 ***************************");
		url = Testo.IX_DISCOVERY_SERVICES_URL + aoos.trim() + "/services/" + type.trim();
		webService = new WebServiceClient(url, token);
		result = webService.get(Testo.TYPE_SERVICES, null);

		//FASE7 - SOTTOSCRIZIONE
		logger.debug(TAG, "********************* FASE 7 ***************************");

		String notificationUrl = null;
		String deleteSubscriptionUrl = null;
		String moveBookmarkUrl = null;
		String confirmBookmarkUrl = null;
		switch (attPass) {
		case Testo.ATTIVA:
			notificationUrl = Testo.IX_ACTIVE_URL;
			deleteSubscriptionUrl = Testo.IX_DELETE_ACTIVE_SUBSCRIPTION;
			moveBookmarkUrl = Testo.IX_MOVE_ACTIVE_BOOKMARK;
			confirmBookmarkUrl = Testo.IX_ACTIVE_URL;
			break;
		case Testo.PASSIVA:
			notificationUrl = Testo.IX_PASSIVE_URL;
			deleteSubscriptionUrl = Testo.IX_DELETE_PASSIVE_SUBSCRIPTION;
			moveBookmarkUrl = Testo.IX_MOVE_PASSIVE_BOOKMARK;
			confirmBookmarkUrl = Testo.IX_PASSIVE_URL;
			break;
		default:
			break;
		}

		String subscribeRequest	= null;
		String subscriptionDate = getSubscriptionDate();
		if (subscriptionDate==null){
			subscribeRequest = "{}";
		} else {
			subscribeRequest = subscriptionDate;
		}
		
		//
		url = notificationUrl + aoos.trim() + "/uos/" + uos.trim() + "/subscriptions";
		webService = new WebServiceClient(url, subscribeRequest);
		subscriptionId = webService.post(token, Testo.TYPE_SUBSCRIPTION);
	
	//loop download	
		boolean run = true;
		while (run){
			String ackUid = download(attPass);
			if(ackUid == null){
 				run = false;
				break;
			}
			//FASE11 - SPOSTO SEGANLIBRO
			logger.debug(TAG, "********************* FASE 10 ***************************");
			url = moveBookmarkUrl + aoos.trim() + "/uos/" + uos.trim() + "/subscriptions/" + subscriptionId + "/acks/" + ackUid;
			webService = new WebServiceClient(url, type+token+aoos);
			result = webService.post(token, Testo.TYPE_MOVE_BOOKMARK);
			
		}
	 			
		//FASE12 - COMMIT SEGNALIBRO
		logger.debug(TAG, "********************* FASE 11 ***************************");
//		url = "https://ixapi.arxivar.it/IxFeApiV3_Transmission_Notification/api/v3/transmission/notifications/aoos/" + aoos.trim() + "/uos/" + uos.trim() + "/subscriptions/" + subscriptionId;
		url = confirmBookmarkUrl + aoos.trim() + "/uos/" + uos.trim() + "/subscriptions/" + subscriptionId;
		webService = new WebServiceClient(url, token);
		result = webService.post(token, Testo.TYPE_COMMIT_BOOKMARK);
		
		//FASE12 - DELETE SUBSCRIPTION
		logger.debug(TAG, "********************* FASE 12 ***************************");
		url = deleteSubscriptionUrl + aoos.trim() + "/uos/" + uos.trim() + "/subscriptions/" + subscriptionId;
		webService = new WebServiceClient(url, token);
		result = webService.post(token, Testo.TYPE_SUBSCRIPTION_DELETE);
	
	}
	
	
	public String download(String attPass){
		
		String notificationUrl = null;
		String fileUrl = null;
		String zipUrl = null;
		String unwrappingUrl = null;
		
		switch (attPass) {
		case Testo.ATTIVA:
			notificationUrl = Testo.IX_ACTIVE_URL;
			break;
		case Testo.PASSIVA:
			notificationUrl = Testo.IX_PASSIVE_URL;
			break;
		default:
			break;
		}
		
		//FASE8 - RICEZIONE NOTIFICHE
		logger.debug(TAG, "********************* FASE 8 ***************************");
		String url = notificationUrl + aoos.trim() + "/uos/" + uos.trim() + "/subscriptions/" + subscriptionId + "/notifications?take=10";
		WebServiceClient webService = new WebServiceClient(url, token);
		String result = webService.get(Testo.TYPE_TRASMISSION, null);
		if (result == null){
			return null;
		}

		Type jsonType = new TypeToken<Collection<Notification>>(){}.getType();
		List<Notification> notifications = (List<Notification>)new Gson().fromJson(result, jsonType);
		
		logger.debug(TAG, "********************* FASE 9 ***************************");
	
		for(Notification notification : notifications) {
			
			String invoiceId = notification.getInvoiceId();
			String idSdi = notification.getIdSdi();
			String dateSdi = notification.getDateSdi();
			String ackId = notification.getAckId();
			
			IX ix = new IX();
			//FASE9 - UPLOAD FILE
			logger.info(TAG, "Download XML file invoiceID: " + invoiceId);

			switch (attPass) {
			case Testo.ATTIVA:
				fileUrl = Testo.IX_DOWNLOAD_ACTIVE_URL + aoos.trim() + "/uos/" + uos.trim() + "/invoices/" + invoiceId + "/file";
				zipUrl = Testo.IX_DOWNLOAD_ACTIVE_URL + aoos.trim() + "/uos/" + uos.trim() + "/invoices/" + invoiceId + "/zip";
				unwrappingUrl = Testo.IX_UNWRAPPING_ACTIVE_URL + aoos.trim() + "/uos/" + uos.trim() + "/invoices/" + invoiceId + "/file";
				break;
			case Testo.PASSIVA:
				fileUrl = Testo.IX_DOWNLOAD_PASSIVE_URL + aoos.trim() + "/uos/" + uos.trim() + "/invoices/" + invoiceId + "/file";
				zipUrl = Testo.IX_DOWNLOAD_PASSIVE_URL + aoos.trim() + "/uos/" + uos.trim() + "/invoices/" + invoiceId + "/zip";
				unwrappingUrl = Testo.IX_UNWRAPPING_PASSIVE_URL + aoos.trim() + "/uos/" + uos.trim() + "/invoices/" + invoiceId + "/file";
				break;
			default:
				break;
			} 
			
			if (attPass.equals(Testo.ATTIVA)){
				webService = new WebServiceClient(zipUrl, token);
				result = webService.get(Testo.TYPE_DOWNLOAD_ZIP, invoiceId);				
			}

			webService = new WebServiceClient(unwrappingUrl, token);
			String nomeXML = webService.get(Testo.TYPE_DOWNLOAD_XML, invoiceId);

			ix.setToken(token);
			ix.setAoos(aoos);
			ix.setUos(uos);
			ix.setSubscription(subscriptionId);
			ix.setInvoice(invoiceId);
			ix.setNomeXml(nomeXML);
			ix.setDateSdi(dateSdi);
			ix.setIdSdi(idSdi);
			ix.setAck(ackId);
			fattureIX.add(ix); 

		}
		
		String ackUid = notifications.get(notifications.size()-1).getAckId();
		return ackUid;
	}

	public String getSubscriptionDate(){
//		"{\"invoiceNotificationUtcDateOffset\":\"2019-11-01T00:00:00.000Z\"}";
		int days = Application.getInstance().getSubbedDays();
		
		if (days == 0){
			return null;
		}

		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		if (days > 0){
			cal.add(Calendar.DATE, -days);
			cal.set(Calendar.HOUR, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
		}  
		
		String dataSubsciption = sdfSubscription.format(cal.getTime());
		
		JsonParser parser = new JsonParser();
		JsonObject o = parser.parse("{\"invoiceNotificationUtcDateOffset\":" +  "\"" + dataSubsciption + "\"}").getAsJsonObject();
		
		return o.toString();
	}
	
	public void removeBOM(File xmlFile) throws Exception{
		/* Cancello BOM da XML */
		FileInputStream inputStream = new FileInputStream(xmlFile);
		BOMInputStream bomInputStream = new BOMInputStream(inputStream);
		String contents = IOUtils.toString(bomInputStream, "UTF-8");
		FileUtils.writeStringToFile(xmlFile, contents,  "UTF-8");
		/* Cancello BOM da XML */
	}
	

}
