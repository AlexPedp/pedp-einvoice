package com.progettoedp.net;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.input.BOMInputStream;
import org.bouncycastle.util.encoders.Base64;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.progettoedp.data.Identifier;
import com.progettoedp.data.Notification;
import com.progettoedp.logic.Unzip;
import com.progettoedp.main.Application;
import com.progettoedp.testo.Testo;
import com.progettoedp.util.Logger;

public class WebServiceClient {

	private static final Logger logger = Logger.getLogger(WebServiceClient.class);
	private static final String TAG = "WebServiceClient";

	private String webUrl;
	private String params;

	public WebServiceClient(String webUrl, String params){
		this.webUrl = webUrl;
		this.params = params;
	}

	public String post(String token, String type){

		byte[] postDataBytes = null;
		List<Identifier> identifiers = new ArrayList<Identifier>();

		try {
			URL url = new URL(webUrl);
			if (params != null) {
				postDataBytes = params.toString().getBytes("UTF-8");
			}

			URLConnection conn = url.openConnection();
			HttpURLConnection http = (HttpURLConnection)conn;
			http.setRequestProperty("Accept", "application/json");
			
			switch (type) {
			case Testo.TYPE_SUBSCRIPTION_DELETE:
				http.setRequestMethod(Testo.DELETE); 
				break;
			case Testo.TYPE_COMMIT_BOOKMARK:
				http.setRequestMethod(Testo.PUT); 
				break;
			default:
				http.setRequestMethod(Testo.POST);  
			}
			
			http.setDoOutput(true);
			conn.setRequestProperty("Content-Type", "application/json");
			if(token != null){

				switch (type) {
				case Testo.TYPE_IDENTIFY:
				case Testo.TYPE_COMMIT_BOOKMARK:
					conn.setRequestProperty ("X-Authorization", token);
					break;
				case Testo.TYPE_REGISTRY:
				case Testo.TYPE_ABILITATION:
				case Testo.TYPE_SUBSCRIPTION_DELETE:
				case Testo.TYPE_MOVE_BOOKMARK:
					conn.setRequestProperty ("X-Authorization", token);
					break;
				case Testo.TYPE_SUBSCRIPTION:
					conn.setRequestProperty ("X-Authorization", token);
					break;
				default:
					break;
				}

			}

			if (postDataBytes!=null){
				conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
				conn.getOutputStream().write(postDataBytes);
			}else {
				byte[] dataByte = Base64.encode(("IXFE" + token.trim()).getBytes());
				conn.setRequestProperty("Content-Length",  String.valueOf(dataByte.length));
			}

			int status = http.getResponseCode();
			

			if (status != HttpURLConnection.HTTP_OK){
				logger.debug(TAG, Integer.toString(status));
				if (http.getErrorStream() != null){
					Reader in = new BufferedReader(new InputStreamReader(http.getErrorStream(), "UTF-8"));
					StringBuilder sb = new StringBuilder();
					for (int c; (c = in.read()) >= 0;)
						sb.append((char)c);
					String response = sb.toString();
					logger.debug(TAG, "Response: " + response);
					return null;	
				} else {
					logger.debug(TAG, "Response code: " + status);
					return null;	
				}

			}

			Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
			StringBuilder sb = new StringBuilder();
			for (int c; (c = in.read()) >= 0;)
				sb.append((char)c);
			String response = sb.toString();
			logger.debug(TAG, "Response: " + response);

			String result = null;

			JsonParser jsonParser = null;
			JsonObject jo = null;
			JsonArray jsonArr = null;
			Iterator <JsonElement> itr = null;
			Map<String, Object> jsonMap  = null;

			switch (type) {
			case Testo.TYPE_AUTH:
				jsonMap = new Gson().fromJson(response, Map.class);
				Object auth = jsonMap.get("access_token");
				logger.debug(TAG, "Post token: " + auth.toString());
				result = auth.toString();
				break;

			case Testo.TYPE_IDENTIFY:
				jsonParser = new JsonParser();
				jo = (JsonObject)jsonParser.parse(response);
				jsonArr = jo.getAsJsonArray("aoos");
				itr = jsonArr.iterator();
				while (itr.hasNext()) {
					JsonObject jsonObject = (JsonObject) itr.next();
					String id  = jsonObject.get("identifier").getAsString();
					String name  = jsonObject.get("name").getAsString();
					JsonObject infos =  jsonObject.getAsJsonObject("identifiers");
					String partitaIva  = infos.get("vatNumber").getAsString();
					Identifier identifier = new Identifier();
					identifier.setIdentifier(id);
					identifier.setName(name);
					identifier.setPartitaIva(partitaIva);
					identifiers.add(identifier);
					logger.debug(TAG, "Identifier " + id +  "|" + "Name " + name + "|" + "vat" + partitaIva);
				}
				
				String json = new Gson().toJson(identifiers);
				result = json;
				
				break;

			case Testo.TYPE_REGISTRY:
				jsonParser = new JsonParser();
				jo = (JsonObject)jsonParser.parse(response);
				jsonArr = jo.getAsJsonArray("aooUos");
				itr = jsonArr.iterator();
				while (itr.hasNext()) {
					JsonObject jsonObject = (JsonObject) itr.next();
					String identifier  = jsonObject.get("identifier").getAsString();
					logger.debug(TAG, "Identifier " + identifier);
					result =  identifier;
					break;
				}
				break;

			case Testo.TYPE_ABILITATION:
			case Testo.TYPE_SUBSCRIPTION_DELETE:
			case Testo.TYPE_MOVE_BOOKMARK:
				break;

			case Testo.TYPE_SUBSCRIPTION:
				jsonMap = new Gson().fromJson(response, Map.class);
				Object subscriptionId = jsonMap.get("subscriptionUID");
				logger.debug(TAG, "subscriptionId: " + subscriptionId.toString());
				result = subscriptionId.toString();
				break;

			default:
				break;
			}


			return result;

		} catch (Exception e) {
			logger.error(TAG, e);
			return null;
		}


	}

	public String get(String type, String invoiceId){
		
		List<Notification> notifications = new ArrayList<Notification>();
		String result = null;
		byte[] postDataBytes = null;

		try {
			URL url = new URL(webUrl);

			if (params != null) {
				postDataBytes = params.toString().getBytes("UTF-8");
			}

			URLConnection conn = url.openConnection();
			HttpURLConnection http = (HttpURLConnection)conn;
			http.setRequestProperty("Accept", "application/json");
			http.setRequestMethod(Testo.GET);  
			http.setDoOutput(true);

			switch (type) {
			case Testo.TYPE_DOWNLOAD_ZIP:
			case Testo.TYPE_DOWNLOAD_XML:
				conn.setRequestProperty("Content-Type", "application/octet-stream");
				break;
			default:
				conn.setRequestProperty("Content-Type", "application/json");
			}

			conn.setRequestProperty ("X-Authorization", params);

			int status = http.getResponseCode();
			String nomeXML = null;

			if (status != HttpURLConnection.HTTP_OK){
				Reader in = new BufferedReader(new InputStreamReader(http.getErrorStream(), "UTF-8"));
				StringBuilder sb = new StringBuilder();
				for (int c; (c = in.read()) >= 0;)
					sb.append((char)c);
				String response = sb.toString();
				logger.debug(TAG, "Response: " + response);
				return null;
			} else {
				switch (type) {
				case Testo.TYPE_DOWNLOAD_ZIP:
					
						File currentDir = new File(".");
					    String fileName = "./zip/" + invoiceId + ".zip";
						InputStream in = conn.getInputStream();
					    FileOutputStream out = new FileOutputStream(fileName);
				        byte[] b = new byte[1024];
				        int count;
				        while ((count = in.read(b)) >= 0) {
				            out.write(b, 0, count);
				        }
				        out.flush(); out.close(); in.close(); 
				        
				        Unzip unzip = new Unzip();
				        nomeXML = unzip.run(new File(fileName));
				        break;
				        
				case Testo.TYPE_DOWNLOAD_XML:
					
					String raw = conn.getHeaderField("Content-Disposition");
					if(raw != null && raw.indexOf("=") != -1) {
						nomeXML = raw.split("=")[1]; 
					} 
				 	
					 try (InputStream inputStream = conn.getInputStream()) {
						 String xmlBasePath = Application.getInstance().getXmlPath();
			                String saveFilePath = xmlBasePath + nomeXML;

			                try (FileOutputStream outputStream = new FileOutputStream(saveFilePath)) {
			                    int bytesRead = -1;
			                    byte[] buffer = new byte[1024];
			                    while ((bytesRead = inputStream.read(buffer)) != -1) {
			                        outputStream.write(buffer, 0, bytesRead);
			                    }
			                }
			                
					 }
					 break;
		 
				}
			}
			
			switch (type) {
			case Testo.TYPE_DOWNLOAD_XML:
				 result = nomeXML;
				 return result;
			}

			Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
			StringBuilder sb = new StringBuilder();
			for (int c; (c = in.read()) >= 0;)
				sb.append((char)c);
			String response = sb.toString();
			logger.debug(TAG, "Response: " + response);

			JsonParser jsonParser = null;
			JsonObject jo = null;
			JsonArray jsonArr = null;
			Iterator <JsonElement> itr = null;
			


			switch (type) {
			case Testo.TYPE_AUTH:
				jsonParser = new JsonParser();
				jo = (JsonObject)jsonParser.parse(response);
				jsonArr = jo.getAsJsonArray("apis");
				itr = jsonArr.iterator();
				while (itr.hasNext()) {
					JsonObject jsonObject = (JsonObject) itr.next();
					String baseUri  = jsonObject.get("baseUri").getAsString();
					String api  = jsonObject.get("api").getAsString();
					logger.debug(TAG, "Response " + baseUri);
					result = baseUri;
					break;
				}
				break;
			case Testo.TYPE_SERVICES:
				jsonParser = new JsonParser();
				jo = (JsonObject)jsonParser.parse(response);
				jsonArr = jo.getAsJsonArray("apis");
				itr = jsonArr.iterator();
				while (itr.hasNext()) {
					JsonObject jsonObject = (JsonObject) itr.next();
					String baseUri  = jsonObject.get("baseUri").getAsString();
					String api  = jsonObject.get("api").getAsString();
					logger.debug(TAG, "Response " + baseUri);
					result = baseUri;
				}
				break;
			case Testo.TYPE_DOWNLOAD_ZIP:
			case Testo.TYPE_DOWNLOAD_XML:
				result = nomeXML;
				break;
			case Testo.TYPE_TRASMISSION:
				jsonParser = new JsonParser();
				jo = (JsonObject)jsonParser.parse(response);
				jsonArr = jo.getAsJsonArray("notifications");
				if (jsonArr.size() == 0 &&  jo.get("ackUID").isJsonNull()){
					return null;
				}
				String ackUID  = jo.get("ackUID").getAsString();
				
				itr = jsonArr.iterator();
				while (itr.hasNext()) {
					Notification notification = new Notification();
					JsonObject jsonObject = (JsonObject) itr.next();
					String invoiceUID  = jsonObject.get("invoiceUID").getAsString();
					String idSdi  = jsonObject.get("exchangeSystemUID").getAsString();
					String dateSdi = jsonObject.get("utcDate").getAsString();
					notification.setInvoiceId(invoiceUID);
					notification.setIdSdi(idSdi);
					notification.setDateSdi(dateSdi);
					notification.setAckId(ackUID);
					notifications.add(notification);
					logger.debug(TAG, "InvoiceId " + invoiceUID + "|" + "idSdi "+ idSdi + "|" + "dateSdi " + dateSdi + "|" + "ack " + ackUID);
				}
//				result = StringUtils.join(notifications, ",");
				String json = new Gson().toJson(notifications);
				result = json;
				break;
			}

			return result;


		} catch (Exception e) {
			logger.error(TAG, e);
			return null;

		}

	}

	public String takeOffBOM(InputStream inputStream) throws IOException {
	    BOMInputStream bomInputStream = new BOMInputStream(inputStream);
	    return IOUtils.toString(bomInputStream, "UTF-8");
	}
	
	public void put(String token){
		
		byte[] postDataBytes = null;
		
		try {
		    URL url = new URL(webUrl);
		    
		    if (params != null) {
				postDataBytes = params.toString().getBytes("UTF-8");
			}
		    
		    HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();
		    httpCon.setDoOutput(true);
		    httpCon.setRequestProperty ("X-Authorization", token);
		    httpCon.setRequestProperty("Content-Type", "application/json");
		    httpCon.setRequestProperty("Accept-Encoding", "application/json");
		    httpCon.setRequestMethod("PUT");
		    
		    if (postDataBytes!=null){
		    	httpCon.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
		    	httpCon.getOutputStream().write(postDataBytes);
			}else {
				byte[] dataByte = Base64.encode(("IXFE" + params.trim()).getBytes());
				httpCon.setRequestProperty("Content-Length",  String.valueOf(dataByte.length));
			}
		    
		    int status = httpCon.getResponseCode();
		    
		    if (status != HttpURLConnection.HTTP_OK){
				logger.debug(TAG, Integer.toString(status));
				if (httpCon.getErrorStream() != null){
					Reader in = new BufferedReader(new InputStreamReader(httpCon.getErrorStream(), "UTF-8"));
					StringBuilder sb = new StringBuilder();
					for (int c; (c = in.read()) >= 0;)
						sb.append((char)c);
					String response = sb.toString();
					logger.debug(TAG, "Response: " + response);
					 	
				} else {
					logger.debug(TAG, "Response code: " + status);
					 	
				}

			}
		    
		   
		} catch (MalformedURLException e) {
		    e.printStackTrace();
		} catch (ProtocolException e) {
		    e.printStackTrace();
		} catch (IOException e) {
		    e.printStackTrace();
		}
	}

}
