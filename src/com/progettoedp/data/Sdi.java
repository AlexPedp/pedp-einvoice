package com.progettoedp.data;

 
 

public class Sdi {
	
	String tipo;
	String dataRitiro;
	String dataSdi1;
	String dataSdi2;
	String nomeXml;
	String numeroDoc;
	String dataDoc;
	String partitaIvaDoc;
	String partitaIvaCli;
	String idSdi; 
	String dataRicezione;
	String dataConsegna;
	  
	public String getNomeXml() {
		return nomeXml;
	}
	public void setNomeXml(String nomeXml) {
		this.nomeXml = nomeXml;
	}
	public String getDataRitiro() {
		return dataRitiro;
	}
	public void setDataRitiro(String dataRitiro) {
		this.dataRitiro = dataRitiro;
	}
	public String getDataSdi1() {
		return dataSdi1;
	}
	public void setDataSdi1(String dataSdi1) {
		this.dataSdi1 = dataSdi1;
	}
	public String getDataSdi2() {
		return dataSdi2;
	}
	public void setDataSdi2(String dataSdi2) {
		this.dataSdi2 = dataSdi2;
	}
	public String getDataDoc() {
		return dataDoc;
	}
	public void setDataDoc(String dataDoc) {
		this.dataDoc = dataDoc;
	}
	public String getPartitaIvaDoc() {
		return partitaIvaDoc;
	}
	public void setPartitaIvaDoc(String partitaIvaDoc) {
		this.partitaIvaDoc = partitaIvaDoc;
	}
	public String getIdSdi() {
		return idSdi;
	}
	public void setIdSdi(String idSdi) {
		this.idSdi = idSdi;
	}
	public String getDataRicezione() {
		return dataRicezione;
	}
	public void setDataRicezione(String dataRicezione) {
		this.dataRicezione = dataRicezione;
	}
	public String getDataConsegna() {
		return dataConsegna;
	}
	public void setDataConsegna(String dataConsegna) {
		this.dataConsegna = dataConsegna;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getNumeroDoc() {
		return numeroDoc;
	}
	public void setNumeroDoc(String numeroDoc) {
		this.numeroDoc = numeroDoc;
	}
	public String getPartitaIvaCli() {
		return partitaIvaCli;
	}
	public void setPartitaIvaCli(String partitaIvaCli) {
		this.partitaIvaCli = partitaIvaCli;
	}
	 
}
