package com.progettoedp.data;

public class TagType {
	
	boolean repeated;
	String type;
	
	public boolean isRepeated() {
		return repeated;
	}
	public void setRepeated(boolean repeated) {
		this.repeated = repeated;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	

}
