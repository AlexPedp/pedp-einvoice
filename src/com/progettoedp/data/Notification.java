package com.progettoedp.data;

public class Notification {
	
	String invoiceId;
	String idSdi;
	String dateSdi;
	String ackId;
	
	public String getInvoiceId() {
		return invoiceId;
	}
	public void setInvoiceId(String invoiceId) {
		this.invoiceId = invoiceId;
	}
	public String getIdSdi() {
		return idSdi;
	}
	public void setIdSdi(String idSdi) {
		this.idSdi = idSdi;
	}
	public String getDateSdi() {
		return dateSdi;
	}
	public void setDateSdi(String dateSdi) {
		this.dateSdi = dateSdi;
	}
	public String getAckId() {
		return ackId;
	}
	public void setAckId(String ackId) {
		this.ackId = ackId;
	}

}
