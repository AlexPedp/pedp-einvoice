package com.progettoedp.data;

public class Riepilogo {
	
	String iva;
	Double imponibile;
	Double imposta;
	String esigibilitaIva;
	String natura;
	
	public String getIva() {
		return iva;
	}
	public void setIva(String iva) {
		this.iva = iva;
	}
	public Double getImponibile() {
		return imponibile;
	}
	public void setImponibile(Double imponibile) {
		this.imponibile = imponibile;
	}
	public Double getImposta() {
		return imposta;
	}
	public void setImposta(Double imposta) {
		this.imposta = imposta;
	}
	public String getEsigibilitaIva() {
		return esigibilitaIva;
	}
	public void setEsigibilitaIva(String esigibilitaIva) {
		this.esigibilitaIva = esigibilitaIva;
	}
	public String getNatura() {
		return natura;
	}
	public void setNatura(String natura) {
		this.natura = natura;
	}
	
}
