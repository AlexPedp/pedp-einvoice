package com.progettoedp.data;

import java.util.ArrayList;
import java.util.List;

public class Tag {
	
	public TagType tagType;
	public List<String> elements;
	
	public Tag(){
		elements = new ArrayList<String>();
	}

	public List<String> getElements() {
		return elements;
	}

	public void setElements(List<String> elements) {
		this.elements = elements;
	}
	
	public void add(String element){
		elements.add(element);
	}
	
	public void clear(){
		elements.clear();
	}

	public TagType getTagType() {
		return tagType;
	}

	public void setTagType(TagType tagType) {
		this.tagType = tagType;
	}
 
}
