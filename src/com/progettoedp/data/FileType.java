package com.progettoedp.data;

public class FileType {

	public enum TYPE {
		PDF,
		ATTACHMENT,
		ALL,
		HTML,
		TEMP;
	}
	
}
