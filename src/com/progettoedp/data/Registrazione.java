package com.progettoedp.data;

public class Registrazione {
	
	private String numeroProtocollo;
	private String dataProtocollo;
	private String numeroProtocolloCollegato;
	private String numeroRegistrazione;
	private String dataRegistrazione;
	private String registroIva;
	
	public String getNumeroProtocollo() {
		return numeroProtocollo;
	}
	public void setNumeroProtocollo(String numeroProtocollo) {
		this.numeroProtocollo = numeroProtocollo;
	}
	public String getDataProtocollo() {
		return dataProtocollo;
	}
	public void setDataProtocollo(String dataProtocollo) {
		this.dataProtocollo = dataProtocollo;
	}
	public String getNumeroProtocolloCollegato() {
		return numeroProtocolloCollegato;
	}
	public void setNumeroProtocolloCollegato(String numeroProtocolloCollegato) {
		this.numeroProtocolloCollegato = numeroProtocolloCollegato;
	}
	public String getNumeroRegistrazione() {
		return numeroRegistrazione;
	}
	public void setNumeroRegistrazione(String numeroRegistrazione) {
		this.numeroRegistrazione = numeroRegistrazione;
	}
	public String getDataRegistrazione() {
		return dataRegistrazione;
	}
	public void setDataRegistrazione(String dataRegistrazione) {
		this.dataRegistrazione = dataRegistrazione;
	}
	public String getRegistroIva() {
		return registroIva;
	}
	public void setRegistroIva(String registroIva) {
		this.registroIva = registroIva;
	}
	
 
}
