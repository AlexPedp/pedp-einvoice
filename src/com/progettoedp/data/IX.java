package com.progettoedp.data;

public class IX {
	
	String token;
	String aoos;
	String uos;
	String subscription;
	String invoice;
	String nomeXml;
	String idSdi;
	String dateSdi;
	String ack;
	
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getAoos() {
		return aoos;
	}
	public void setAoos(String aoos) {
		this.aoos = aoos;
	}
	public String getUos() {
		return uos;
	}
	public void setUos(String uos) {
		this.uos = uos;
	}
	public String getSubscription() {
		return subscription;
	}
	public void setSubscription(String subscription) {
		this.subscription = subscription;
	}
	public String getInvoice() {
		return invoice;
	}
	public void setInvoice(String invoice) {
		this.invoice = invoice;
	}
	public String getNomeXml() {
		return nomeXml;
	}
	public void setNomeXml(String nomeXml) {
		this.nomeXml = nomeXml;
	}
	public String getIdSdi() {
		return idSdi;
	}
	public void setIdSdi(String idSdi) {
		this.idSdi = idSdi;
	}
	public String getDateSdi() {
		return dateSdi;
	}
	public void setDateSdi(String dateSdi) {
		this.dateSdi = dateSdi;
	}
	public String getAck() {
		return ack;
	}
	public void setAck(String ack) {
		this.ack = ack;
	}
	
}
