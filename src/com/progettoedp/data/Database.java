package com.progettoedp.data;

public class Database {
	
	private String type;
	private String host;
	private String name;
	private String instance;
	private String port;
	private String user;
	private String password;
	
	
	public Database(String type, String host,  String name, String instance, String port, String user, String password){
		this.type = type;
		this.host = host;
		this.name = name;
		this.instance = instance;
		this.port = port;
		this.user = user;
		this.password = password;
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getInstance() {
		return instance;
	}
	public void setInstance(String instance) {
		this.instance = instance;
	}
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}

	 

}
