package com.progettoedp.receipt;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.io.FilenameUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.progettoedp.data.Fattura;
import com.progettoedp.testo.Testo;
import com.progettoedp.util.Logger;

public class XMLReceipt {

	private static final Logger logger = Logger.getLogger(XMLReceipt.class);
	private static final String TAG = "XMLReceipt";
	private File file;
	private Fattura fattura;

	public XMLReceipt(File file, Fattura fattura){
		this.file = file;
		this.fattura = fattura;
	}

	public File run(){ 
		 
		File receiptFile = new File(FilenameUtils.removeExtension(file.getName()) + "_RC" + "." + Testo.XML);
		
		String dataAccettazioneSdi = null;
		String dataConsegnaSdi = null;
		if (fattura.getDataRitiro()!= null){
			dataAccettazioneSdi = fattura.getDataRitiro();
		} else {
			dataAccettazioneSdi = fattura.getDataSdi2();
		}
		if (fattura.getDataCon()!= null){
			dataConsegnaSdi = fattura.getDataCon();
		} else {
			dataConsegnaSdi = dataAccettazioneSdi;
		}
		
		
		try {

			DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();

			DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();

			Document document = documentBuilder.newDocument();
			
			Element root = document.createElement("Ricevuta");
	        document.appendChild(root);
 
			Element idSdi = document.createElement("IdentificativoSdI");
			idSdi.appendChild(document.createTextNode(fattura.getIdSdi().trim()));
			root.appendChild(idSdi);
			
			Element nomeXml = document.createElement("NomeFile");
			nomeXml.appendChild(document.createTextNode(fattura.getNomeXml().trim()));
			root.appendChild(nomeXml);
		 
			Element dataRicezione = document.createElement("DataOraRicezione");
			dataRicezione.appendChild(document.createTextNode(dataAccettazioneSdi));
			root.appendChild(dataRicezione);
			
			Element dataConsegna = document.createElement("DataOraConsegna");
			dataConsegna.appendChild(document.createTextNode(dataConsegnaSdi));
			root.appendChild(dataConsegna);

			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource domSource = new DOMSource(document);
			StreamResult streamResult = new StreamResult(receiptFile);
 
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
			transformer.transform(domSource, streamResult);

			logger.info(TAG, "Receipt file created correctly: " + receiptFile.getName());
			
			return receiptFile;

		} catch (Exception e) {
			logger.error(TAG, "Error: " + e);
			return null;
		} 
	}
 

}
