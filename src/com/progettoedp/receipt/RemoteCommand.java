package com.progettoedp.receipt;

import com.ibm.as400.access.AS400;
import com.ibm.as400.access.AS400Message;
import com.ibm.as400.access.CommandCall;
import com.ibm.as400.access.ProgramCall;
import com.progettoedp.testo.Testo;
import com.progettoedp.util.Logger;

public class RemoteCommand {

	private static final Logger logger = Logger.getLogger(RemoteCommand.class);
	private static final String TAG = "RemoteCommand";
 
	private String azienda;
	private String host;
	private String uid;
	private String pwd;

	public RemoteCommand(String azienda, String host, String uid, String pwd){
		this.azienda=azienda;
		this.host = host;
		this.uid = uid;
		this.pwd = pwd;
	}


	public void run() {

		String fullProgramName = "/QSYS.LIB/ACK63L_OBJ.LIB/FA014CL.PGM";

		AS400 as400 = null;
	 	ProgramCall programCall;
		try {

			as400 = new AS400(host, uid, pwd);
		 
			programCall = new ProgramCall(as400);
			programCall.setProgram(fullProgramName);

			CommandCall command = new CommandCall(as400);

			command.run("ADDLIBLE LIB(GA_V02_O)");
			command.run("ADDLIBLE LIB(DA_V02_O)");
			command.run("ADDLIBLE LIB(PCNFE_BAS)");
			command.run("ADDLIBLE LIB(KEYTOOL)");
			command.run("ADDLIBLE LIB(PLEX510)");
			command.run("ADDLIBLE LIB(ACK63L_OBJ)");
			command.run("ADDLIBLE LIB(ACK63L_OBP)");
			command.run("ADDLIBLE LIB(BMS63L_TON)");
			command.run("ADDLIBLE LIB(KMS63L_TON)");
			command.run("ADDLIBLE LIB(IT_V02_S)");
			command.run("ADDLIBLE LIB(IT_V02_O)");
			
			switch (azienda) {
			case Testo.TONIOLO:
				command.run("ADDLIBLE LIB(IT_CTO_F)");
				command.run("ADDLIBLE LIB(IT_CTO_PO)");
				command.run("ADDLIBLE LIB(IT_CTO_PF)");	
				break;
			case Testo.TONIOLO_SDM:
				command.run("ADDLIBLE LIB(IT_SDM_F)");
				command.run("ADDLIBLE LIB(IT_SDM_PO)");
				command.run("ADDLIBLE LIB(IT_SDM_PF)");	
				break;
			case Testo.ALBIERO:
				command.run("ADDLIBLE LIB(IT_ALB_F)");
				command.run("ADDLIBLE LIB(IT_ALB_PO)");
				command.run("ADDLIBLE LIB(IT_ALB_PF)");	
				break;
			default:
				break;
			}
			
			command.run("ADDLIBLE LIB(QTEMP)");
			command.run("ADDLIBLE LIB(QGPL)");

			if (!programCall.run()) {
				AS400Message[] messageList = programCall.getMessageList();
				for (AS400Message message : messageList) {
					logger.error(TAG, "Remote command AS400 run with error: " + message.getID() + " - " + message.getText());
				}
			} else {
				logger.info(TAG, "Remote command AS400 run successfully");
	 		}
			as400.disconnectService(AS400.COMMAND);

		} catch (Exception e) {
			logger.error(TAG, "Remote command AS400 run with error: " + e );
		} finally {
			try {
				if (as400 != null) {
					as400.disconnectAllServices();
				}
			} catch (Exception e) {
				logger.error(TAG, "Remote command AS400 run with error: " + e );
			}
		}
	}


}
