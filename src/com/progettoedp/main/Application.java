package com.progettoedp.main;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.jdesktop.application.SingleFrameApplication;

import com.progettoedp.dao.DAO;
import com.progettoedp.dao.DatabaseManager;
import com.progettoedp.data.Database;
import com.progettoedp.logic.Import;
import com.progettoedp.logic.ImportIX;
import com.progettoedp.properties.AppProperties;
import com.progettoedp.testo.Testo;


public class Application extends SingleFrameApplication{

	private DAO dao;
	private String applicationType;
	private String hostName;
	private String portNumber;
	private String databaseName;
	private String user;
	private String password;
	private String partitaIva;
	private String xmlPath;
	private String pdfPath;
	private String attachPath;
	private String backupPath;
	private String tableName;
	private String tableNameEq;
	private String printerName;
	private String secondPrinterName;
	private String host_as400;
	private String user_as400;
	private String password_as400;
	private String library_as400;
	private String databaseNameEq;
	private int subbedDays;
	private boolean activeInvoice;
	private String azienda;
	private String databaseApplicationType;
	private String databaseApplicationName;
	private String hostApplicationName;
	private String instanceApplicationName;
	private String portApplicationName;
	private String userApplicationName;
	private String passwordApplicationName;
	private String ixUserName;
	private String ixPassword;
	private String ixClientId;
	private String ixClientSecret;
	private Database sqlite;
	private Database mssqlApp;
	private Database mssql;
	private Database mssqlEq;
	private Database as400;
	private AppProperties appProperties;
	private String library_as400_cont;
	private String tipoProtocollo;
	private String xmlAs400Path;
	private boolean ftp_as400;
	private String[] filteredDocuments;
	
	
	private List<Database> databases = new ArrayList<Database>();
	

	public void init()  {

		appProperties = new AppProperties();
		
		readProperties();
		addDatabaseManager();
		
		DatabaseManager databaseManager =  new DatabaseManager(databases);
		dao = DAO.create(databaseManager);
		databaseManager.init();
		
		switch (databaseApplicationType) {
		case Testo.SQLITE:
			dao.init(sqlite);  
			break;
		case Testo.MSSQL:
			dao.init(mssqlApp);  
 			break;

		default:
			break;
		}
	
		try {
			switch (applicationType) {
			case Testo.FATEL:
				Import importa = new Import(dao);
 				importa.run();
 				break;
			case Testo.IX:
				ImportIX importaIx = new ImportIX(azienda, dao);
				importaIx.run();
//				importaIx.runTest();
				break;
			default:
				break;
			}

		} catch (Exception e) {

		}  


	}
	
	public void addDatabaseManager(){
		sqlite = new Database(Testo.SQLITE, null, null, null, null, null, null);
		databases.add(sqlite);
		mssqlApp = new Database(Testo.MSSQL, hostApplicationName, databaseApplicationName, instanceApplicationName, portApplicationName, userApplicationName, passwordApplicationName);
		databases.add(mssqlApp);
		mssql = new Database(Testo.MSSQL, hostName, databaseName, null, portNumber, user, password);
		databases.add(mssql);
		mssqlEq = new Database(Testo.MSSQL, hostName, databaseNameEq, null, portNumber, user, password);
		databases.add(mssqlEq);
		as400 =  new Database(Testo.AS400, host_as400, null,  null, null, user_as400, password_as400);
		databases.add(as400);
	}

	public void readProperties(){
		
		String dir = System.getProperty("user.dir");

		File file = new File(dir + "/config/" + azienda + "/" + "application.properties");
		if(!file.exists()) {
			appProperties.create(file, true);
		}

		appProperties.load(file);
		
		partitaIva = appProperties.partitaIva;
		applicationType = appProperties.applicationType;
		
		databaseApplicationType =  appProperties.databaseApplicationType;
		databaseApplicationName = appProperties.databaseApplicationName;
		hostApplicationName = appProperties.hostApplicationName;
		instanceApplicationName = appProperties.instanceApplicationName;
		portApplicationName = appProperties.portApplicationName;
		userApplicationName =  appProperties.userApplicationName;
		passwordApplicationName =  appProperties.passwordApplicationName;
		hostName = appProperties.hostName;
		portNumber = appProperties.portNumber;
		databaseName = appProperties.databaseName;
		user = appProperties.user;
		password = appProperties.password;
		xmlPath = appProperties.xmlPath;
		pdfPath = appProperties.pdfPath;
		tableName = appProperties.tableName;
		tableNameEq = appProperties.tableNameEq;
		attachPath = appProperties.attachPath;
		backupPath = appProperties.backupPath;
		printerName = appProperties.printerName;
		secondPrinterName = appProperties.secondPrinterName;
		host_as400 =  appProperties.host_as400;
		user_as400 = appProperties.user_as400;
		password_as400 = appProperties.password_as400;
		library_as400 = appProperties.library_as400;
		databaseNameEq =appProperties.databaseNameEq;
		subbedDays = appProperties.subbedDays;
		activeInvoice = appProperties.activeInvoice;
		ixUserName = appProperties.ixUserName;
		ixPassword = appProperties.ixPassword;
		ixClientId = appProperties.ixClientId;
		ixClientSecret = appProperties.ixClientSecret;
		xmlAs400Path = appProperties.xml_as400_path;
		tipoProtocollo = appProperties.tipo_protocollo;
		library_as400_cont = appProperties.library_as400_cont;
		ftp_as400 = appProperties.ftp_as400;
		filteredDocuments = appProperties.filteredDocuments.split("\\|");
	}

 

	@Override
	protected void initialize(String[] args) {
		azienda = args[0];
		super.initialize(args);
	}


	public static synchronized Application getInstance() {
		return (Application) org.jdesktop.application.Application.getInstance();
	}

 
	public DAO getDao() {
		return dao;
	}

	public void setDao(DAO dao) {
		this.dao = dao;
	}

	@Override
	protected void shutdown() {
		System.exit(1);
	}

	@Override
	protected void startup() {
		init();
	}

	public String getApplicationType() {
		return applicationType;
	}


	public String getHostName() {
		return hostName;
	}


	public String getPortNumber() {
		return portNumber;
	}


	public String getDatabaseName() {
		return databaseName;
	}

	public String getUser() {
		return user;
	}


	public String getPassword() {
		return password;
	}

	public String getPartitaIva() {
		return partitaIva;
	}

	public String getXmlPath() {
		return xmlPath;
	}

	public String getPdfPath() {
		return pdfPath;
	}

	public String getTableName() {
		return tableName;
	}

	public String getAttachPath() {
		return attachPath;
	}

	public void setAttachPath(String attachPath) {
		this.attachPath = attachPath;
	}
	
	public String getBackupPath() {
		return backupPath;
	}

	public void setBackupPath(String backupPath) {
		this.backupPath = backupPath;
	}

	public String getPrinterName() {
		return printerName;
	}

	public void setPrinterName(String printerName) {
		this.printerName = printerName;
	}
	
	public String getSecondPrinterName() {
		return secondPrinterName;
	}

	public void setSecondPrinterName(String secondPrinterName) {
		this.secondPrinterName = secondPrinterName;
	}

	public String getHost_as400() {
		return host_as400;
	}

	public void setHost_as400(String host_as400) {
		this.host_as400 = host_as400;
	}

	public String getUser_as400() {
		return user_as400;
	}

	public void setUser_as400(String user_as400) {
		this.user_as400 = user_as400;
	}

	public String getPassword_as400() {
		return password_as400;
	}

	public void setPassword_as400(String password_as400) {
		this.password_as400 = password_as400;
	}

	public String getLibrary_as400() {
		return library_as400;
	}

	public void setLibrary_as400(String library_as400) {
		this.library_as400 = library_as400;
	}

	public String getDatabaseNameEq() {
		return databaseNameEq;
	}

	public void setDatabaseNameEq(String databaseNameEq) {
		this.databaseNameEq = databaseNameEq;
	}

	public int getSubbedDays() {
		return subbedDays;
	}

	public void setSubbedDays(int subbedDays) {
		this.subbedDays = subbedDays;
	}

	public String getTableNameEq() {
		return tableNameEq;
	}

	public void setTableNameEq(String tableNameEq) {
		this.tableNameEq = tableNameEq;
	}

	public boolean isActiveInvoice() {
		return activeInvoice;
	}

	public void setActiveInvoice(boolean activeInvoice) {
		this.activeInvoice = activeInvoice;
	}

	public Database getSqlite() {
		return sqlite;
	}
 
	public Database getMssqlApp() {
		return mssqlApp;
	}

 
	public Database getMssql() {
		return mssql;
	}
  
	public Database getMssqlEq() {
		return mssqlEq;
	}
 
	public Database getAs400() {
		return as400;
	}

	public String getIxUserName() {
		return ixUserName;
	}

	public void setIxUserName(String ixUserName) {
		this.ixUserName = ixUserName;
	}

	public String getIxPassword() {
		return ixPassword;
	}

	public void setIxPassword(String ixPassword) {
		this.ixPassword = ixPassword;
	}

	public String getAzienda() {
		return azienda;
	}

	public String getIxClientId() {
		return ixClientId;
	}

	public void setIxClientId(String ixClientId) {
		this.ixClientId = ixClientId;
	}

	public String getIxClientSecret() {
		return ixClientSecret;
	}

	public void setIxClientSecret(String ixClientSecret) {
		this.ixClientSecret = ixClientSecret;
	}

	public String getDatabaseApplicationType() {
		return databaseApplicationType;
	}

	public String getLibrary_as400_cont() {
		return library_as400_cont;
	}

	public void setLibrary_as400_cont(String library_as400_cont) {
		this.library_as400_cont = library_as400_cont;
	}

	public String getTipoProtocollo() {
		return tipoProtocollo;
	}

	public void setTipoProtocollo(String tipoProtocollo) {
		this.tipoProtocollo = tipoProtocollo;
	}

	public String getXmlAs400Path() {
		return xmlAs400Path;
	}

	public void setXmlAs400Path(String xmlAs400Path) {
		this.xmlAs400Path = xmlAs400Path;
	}

	public boolean isFtp_as400() {
		return ftp_as400;
	}

	public void setFtp_as400(boolean ftp_as400) {
		this.ftp_as400 = ftp_as400;
	}

	public String[] getFilteredDocuments() {
		return filteredDocuments;
	}

	public void setFilteredDocuments(String[] filteredDocuments) {
		this.filteredDocuments = filteredDocuments;
	}
	 
}
