package com.progettoedp.main;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import com.progettoedp.testo.Testo;
import com.progettoedp.ui.MainWindow;
import com.progettoedp.util.Logger;
 

public class Main {

	private static final Logger logger = Logger.getLogger(Main.class);
	private static final String TAG = "Main";
	
	public static void main(String[] args) {
	 	
		if (args.length == 0) {
			logger.info(TAG, "No arguments passed");
			System.exit(3);
		} else {
			for (int i = 0; i < args.length; i++) {
				logger.info(TAG, "Arguments passed: " + args[i]);
			}
		}

    	try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (args.length >= 2 && args[1].equals(Testo.CONFIG_MODE)){
			String azienda = args[0];
			MainWindow window = new MainWindow(azienda);
			window.display();
		} else {
			Application.launch(Application.class, args);	
		}
		
	}
	
}
